difference()
{
	union()
	{
		difference()
		{
			translate([5,11,20.8])
			rotate([0,0,90])
				import("OptiYA.stl");

			translate([-3,3.5,2.4])
				cube([5,3.5,7.25]);

			translate([-3,3.5,20.5])
				cube([5,3.5,7.25]);
		}

		union()
		{
			difference()
			{
				import("SCS8UU_ADIv4.stl");

				translate([17,11,-1])
				   cylinder(d=15, h=32, $fn=30);

				translate([0,0,-2])
					cube([40,40,2]);
			}

			translate([17,11,0])
			{
				id=11;
				id_tol=.3;
				od=18;
				od_tol=0;
				len=30;
				chamfer=.4;

				rotate_extrude(convexity=2, $fn=90)
					polygon([[(id+id_tol)/2+chamfer,0],
							[(id+id_tol)/2,chamfer],
							[(id+id_tol)/2,len-chamfer],
							[(id+id_tol)/2+chamfer,len],
							[(od-od_tol)/2-chamfer,len],
							[(od-od_tol)/2,len-chamfer],
							[(od-od_tol)/2,chamfer],
							[(od-od_tol)/2-chamfer,0]]);
			}
		}
			
		linear_extrude(1, convexity=10)
		polygon([[-1.3,0],[9,0],[9,17],[14,22],[-1.3,22],[-1.3,11.65],[-2.25,11.65],[-2.25,3.8],[-1.3,3.8]]);

		translate([1,7.5,0])
		cube([1,10,30]);

		translate([0.5,17,0])
		cube([9,1,30]);
	}

	translate([5,-1,6])
	rotate([-90,0,0])
		cylinder(d=4.4, h=30, $fn=30);

	translate([5,-1,24])
	rotate([-90,0,0])
		cylinder(d=4.4, h=30, $fn=30);
}