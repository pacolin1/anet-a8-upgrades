difference()
{
	union()
	{
		difference()
		{
			union()
			{
				import("SCS8UU_ADIv4.stl");

				translate([22,11.1,20.5])
				rotate([0,-90,90])
				difference()
				{
					import("OptiXA.stl");
					translate([-25,-18,4.25])
						cube([40,10,10]);
				}
			}

			translate([17,11,-1])
			   cylinder(d=15, h=32, $fn=30);

			translate([0,0,-2])
				cube([40,40,2]);
		}

		translate([17,11,0])
		{
			id=11;
			id_tol=.3;
			od=18;
			od_tol=0;
			len=30;
			chamfer=.4;

			rotate_extrude(convexity=2, $fn=90)
				polygon([[(id+id_tol)/2+chamfer,0],
						[(id+id_tol)/2,chamfer],
						[(id+id_tol)/2,len-chamfer],
						[(id+id_tol)/2+chamfer,len],
						[(od-od_tol)/2-chamfer,len],
						[(od-od_tol)/2,len-chamfer],
						[(od-od_tol)/2,chamfer],
						[(od-od_tol)/2-chamfer,0]]);
		}

		linear_extrude(2, convexity=19)
			polygon([[35,6.8],[42.8,6.8],[42.8,8.8],[35,16.5]]);

		translate([32.3,7,0])
			cube([1,12,30]);

		translate([25,17,0])
			cube([8,1,30]);

		translate([1,16.5,0])
			cube([8,2,30]);
	}


	translate([5,-1,6])
	rotate([-90,0,0])
		cylinder(d=4.4, h=30, $fn=30);

	translate([5,-1,24])
	rotate([-90,0,0])
		cylinder(d=4.4, h=30, $fn=30);
		
	translate([29,-1,6])
	rotate([-90,0,0])
		cylinder(d=4.4, h=30, $fn=30);

	translate([29,-1,24])
	rotate([-90,0,0])
		cylinder(d=4.4, h=30, $fn=30);
}