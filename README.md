Upgrades for the Anet A8
========================

These are sorted into the following directories:

* current: what I'm currently using
* previous: what I've used in the past
* pending: what I might use in the future
* abandoned: gave up on it
* other: calibration objects, non-A8-specific stuff, etc.

The compilation (as its own entity) is licensed under CC0; the various
subprojects all have their own terms that should be called out within.  (For
some, it may be necessary to go to the original Thingiverse project page to
find licenses and other terms.)
