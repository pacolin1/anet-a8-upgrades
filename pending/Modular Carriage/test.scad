use <GT2_tooth.scad>

extra_gap=0.15;

for (i=[0:2:30])
translate([i,.7,4])
    gt2_tooth(7);

translate([-1,1.45,4])
    cube([32,10,7]);

translate([-1,-10-extra_gap,0])
{
    cube([32,21.45+extra_gap,4]);
    cube([32,10,11]);
}