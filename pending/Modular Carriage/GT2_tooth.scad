// modeled after the locks in https://www.thingiverse.com/thing:2849925

module gt2_tooth(width)
{
    translate([-.25,0,0])
        cube([.5,.75,width]);

    translate([-.25,.165,0])
        cylinder(r=.165, h=width, $fn=30);

    translate([.25,.165,0])
        cylinder(r=.165, h=width, $fn=30);

    translate([-1,.165,0])
    difference()
    {
        cube([.75,.585,width]);
        cylinder(r=.585, h=width, $fn=60);
    }

    translate([0.25,.165,0])
    difference()
    {
        cube([.75,.585,width]);
        translate([.75,0,0])
        cylinder(r=.585, h=width, $fn=60);
    }
}

