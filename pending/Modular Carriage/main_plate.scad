//%import("../../previous/X-axis mounts/Anet_A8_X_Axis_Mount_Right.stl");

// gap under lower rod when homed (bed-to-bottom): 31 mm
// spacing between X-axis rods (center-to-center): 46 mm
// stock carriage width: 60 mm

use <LM8UU_housing_3_point_anet_a8.scad>
use <GT2_tooth.scad>

// render these one at a time

backplate();
//tensioner();

// for a 1.4 mm belt and PLA, 1.6 mm seems to be a good starting value
// adjust the "loosen" variable below for other materials or belt thicknesses

module belt_retainer()
{
    loosen=0.15; // increase for a thicker belt or to loosen the fit
    for (i=[0:2:18])
    translate([i+1,1.6+loosen-.75,0])
        gt2_tooth(7);
    translate([0,-4,0])
        cube([20,4,7]);
    translate([0,1.6+loosen,0])
        cube([20,4-loosen,7]);
}

// -------------------------------------------------------------
// rods (not to be rendered)

//translate([4.6,0,7.3])
translate([0,100,35])
rotate([90,0,0])
    %cylinder(d=8, $fn=90, h=200);

translate([0,0,46]) // spacing between X-axis rods
//translate([4.6,0,7.3])
translate([0,100,35])
rotate([90,0,0])
    %cylinder(d=8, $fn=90, h=200);

// -------------------------------------------------------------
// current carriage (not to be rendered)

// translate([11.5,-31,16.5])
// rotate([90,0,90])
// %import("../../current/AM8 mod/e3d-v6-carriage-mods/1_E3DV6_BasePlate_V4_fixed.stl");

// -------------------------------------------------------------
// hotend model (not to be rendered)

translate([0,0,-2.88]) // place on bed
translate([29.7,-5,46.15])
rotate([0,0,-90])
    %import("../../previous/fan_duct_3/E3D_v6_To_Spec.stl");

// -------------------------------------------------------------
// bed-level sensor model (not to be rendered)

translate([-16,0,45.05])
rotate([0,0,-90])
    %import("BLTouch_Model.stl");

module tensioner()
{
    gap=0.35; // adjust for proper fit (try 0.25 for PLA, 0.35 for PETG)

    difference()
    {
        // -------------------------------------------------------------
        // main body

        union()
        {
            translate([-3.5,-30,55])
            rotate([90,0,90])
                belt_retainer();

            translate([3.5,-30,51])
                cube([7.85,20,9.6]);
        }

        // -------------------------------------------------------------
        // cut out notch for rail

        translate([11.5,-30,55.5])
        rotate([90,0,180])
        linear_extrude(20, convexity=10)
            polygon([[0,-1.5-gap],[1.2,-2-gap],[1.2,2+gap],[0,1.8+gap]]);

        // -------------------------------------------------------------
        // drill screw hole

        translate([7,35,55.5])
        rotate([90,0,0])
            cylinder(d=3.5, h=70, $fn=30);
        translate([7,-14,55.5])
        rotate([0,30,0])
        rotate([90,0,0])
            cylinder(d=6.8, h=16, $fn=6);
    }
}

module backplate()
{

    difference()
    {
        union()
        {
            // -------------------------------------------------------------
            // bearing holders

            translate([0,-15.5,81])
            rotate([90,0,0])
                top(4, 15.3, "no", "no", "no", "no");

            translate([0,15.5,81])
            rotate([-90,0,0])
                top(4, 15.3, "no", "no", "no", "no");    


            translate([0,0,35])
            rotate([-90,0,0])
                top(4, 15.3, "no", "no", "no", "no");

            // -------------------------------------------------------------
            // main plate    

           translate([11.5,-30,19])
           cube([4,60,78]);

            // -------------------------------------------------------------
            // right-side belt retainer

            translate([-3.5,10,55])
            rotate([90,0,90])
                belt_retainer();

            translate([3.5,10,51])
                cube([8,20,9.6]);

            // -------------------------------------------------------------
            // track for left-side belt retainer

            translate([11.5,-30,55.5])
            rotate([90,0,180])
            linear_extrude(40, convexity=10)
                polygon([[0,-1.5],[1,-2],[1,2],[0,1.5]]);

            // -------------------------------------------------------------
            // lower mounting hole surrounds

            // for (x=[-25:50:25])
            // translate([7.5,x,24])
            // {
            //     rotate([0,90,0])
            //         cylinder(d=10, $fn=90, h=4);
            // }

            // -------------------------------------------------------------
            // BLTouch mount

            translate([-20.5,-7.5,45.1])
                cube([25,15,3]);
            translate([-16,-7.5,45.1])
                cylinder(d=9, h=3, $fn=60);
            translate([-16,7.5,45.1])
                cylinder(d=9, h=3, $fn=60);

            translate([-8.5,4.5,45.1])
            rotate([-90,0,0])
            linear_extrude(3, convexity=10)
                polygon([[0,0],[0,8],[8,0]]);
            translate([-8.5,-7.5,45.1])
            rotate([-90,0,0])
            linear_extrude(3, convexity=10)
                polygon([[0,0],[0,8],[8,0]]);
        }

        // -------------------------------------------------------------
        // plate mounting holes

        for (x=[-25:50:25])
        for (y=[24:68:92])
        translate([-20,x,y])
        {
            rotate([0,90,0])
                cylinder(d=3.1, $fn=30, h=40);
            rotate([30,0,0])
            rotate([0,90,0])
                cylinder(d=6.2, $fn=60, h=31.5);
        }

        // -------------------------------------------------------------
        // BLTouch mounting holes

        for (y=[-9:18:9])
        translate([-16,y,35])
            cylinder(d=3.1, h=20, $fn=30);
        translate([-16,0,35])
            cylinder(d=6, h=20, $fn=60);

        // -------------------------------------------------------------
        // tensioner holes

        translate([7,35,55.5])
        rotate([90,0,0])
            cylinder(d=3.5, h=70, $fn=30);
        translate([7,30,55.5])
        rotate([90,0,0])
            cylinder(d=6, h=16, $fn=60);

        // -------------------------------------------------------------
        // tensioner clearance

        translate([1.5,-30,50])
            cube([10,40,1]);

        // -------------------------------------------------------------
        // keep bridging from intruding into bearing holders

        translate([-8.45,-35,79.5])
            cube([1,70,3]);
        translate([-8.45,-35,33.5])
            cube([1,70,3]);
    }

}

