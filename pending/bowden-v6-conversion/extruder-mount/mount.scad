$fn=90;

module spacer(t)
{
    linear_extrude(t) // set thickness here
    difference()
    {
        minkowski()
        {
            square(31.3, center=true);
            circle(d=11);
        }
        translate([-15.5,-15.5])
            circle(d=3.4);
        translate([15.5,-15.5])
            circle(d=3.4);
        translate([-15.5,15.5])
            circle(d=3.4);
        translate([15.5,15.5])
            circle(d=3.4);
        circle(d=23);
    }
}

translate([0,0,-15])
difference()
{
    translate([-84.5,.5,-11.5])
    minkowski()
    {
        union()
        {
            cube([169,2,24]);

            translate([0,11,0])
                cube([169,2,24]);

            translate([0,0,24])
                cube([169,13,2]);

        }
        
        sphere(d=1);
    }

    translate([-76,7,0])
    rotate([90,0,0])
        cylinder(d=3.4, h=14, center=true);

    translate([76,7,0])
    rotate([90,0,0])
        cylinder(d=3.4, h=14, center=true);
}

translate([0,0,22])
rotate([-90,0,0])
    spacer(2);

translate([-22.5,0,-1])
    cube([45,2,5]);

translate([-24.5,2,-1])
rotate([90,0,90])
linear_extrude(2)
    polygon([[0,0],[0,24],[12,0]]);

translate([22.5,2,-1])
rotate([90,0,90])
linear_extrude(2)
    polygon([[0,0],[0,24],[12,0]]);

translate([19.5,0,-1])
    cube([5,2,24]);

translate([-24.5,0,-1])
    cube([5,2,24]);
