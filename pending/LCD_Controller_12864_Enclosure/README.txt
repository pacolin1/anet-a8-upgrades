                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2756990
LCD Controller 12864 Enclosure by k4en is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Enclosure for LCD Controller 12864 similar to other designs on Thingiverse. This one is unique from the ones I've seen in that the black metal around the LCD is covered by a .6mm thick mask so only the active portion of the LCD is showing. For similar controller with voltmeter see https://www.thingiverse.com/thing:2759951.

Controller available on Amazon Prime for around $13. Search for "12864 LCD Full Graphic Smart Display Controller".

Use a sharp knife to remove the thin film over the holes for the LCD controls. There are two bases provided to allow for 28 and 45 degree angle on the bezel. 3mm screws secure the LCD to the bezel. The cutout in the back of the base is for the ribbon cables to exit

The example shown has bezel printed in Gun Metal Gray PLA; base in silver; reset button actuator in orange for contrast.