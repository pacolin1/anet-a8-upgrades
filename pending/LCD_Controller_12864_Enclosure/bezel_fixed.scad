$fn=45;

base();

module bezel()
{
    difference()
    {
        translate([0,0,13])
        rotate([180,0,0])
            import("LCD_12864_Front_Bezel_2.stl");

        translate([-.22,36.23,0])
            cylinder(d=5.5, h=10);
        translate([35.7,36,0])
            cylinder(d=9, h=10);
        translate([17.75,36.2,0])
            cylinder(d=2.5, h=10);
    }
}

module base()
{
    difference()
    {
        translate([0,0,13])
        rotate([180,0,0])
            import("Back_2_45_degrees.stl");

        translate([-54,-24,15])
            cube([6,32,8]);
    }
}

module lcd()
{
    translate([0,0,17.0])
    rotate([180,0,0])
        %import("12864.stl");
}

