bar=160;
bar_z = 37;
$fn=90;

// motor mount
union() {
translate([40-bar,-21.5,39])
    rotate([90,0,90])
        import("Y-axis_motor2.1.stl");
}

// other end
translate([70,0,-29])
rotate([0,-90,0])
import("Y-axis_belt2.1.stl");

// roller carriage
translate([10,1,bar_z])
    rotate([0,90,0])
        import("Y-axis belt2 roller.stl");

// bar 1
#translate([50-bar,14,bar_z])
    rotate([0,90,0])
        cylinder(d=6,h=bar);
// bar 2
#translate([100-bar,-12,bar_z])
    rotate([0,90,0])
        cylinder(d=6,h=bar-50);
