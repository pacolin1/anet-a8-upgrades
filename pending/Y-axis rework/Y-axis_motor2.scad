
print = true;

cyl_inner = 6.2;
cyl_outer = 12;

module original() {
    union() {
import("super_waasa-amberis_1.stl");
    }
}


$fn=90;

module arc(diam, thick) {
    union() {
        intersection() {
            difference() {
                cube([diam,diam,thick]);
                translate([diam/2,diam/2,0])
                    cylinder(d=diam,h=thick);
            }
            translate([0,0,0])
                cube([diam/2,diam/2,thick]);
        }
    }
}


module new() {
    
    scale([1,1,7.8/6.8]) {

        difference() {
            union() {
                original();

                // extra tab n chassie
                translate([26.2,-19.5,0])
                    cube([8,11.7,10]);

                // extra for 6mm screw
                translate([-35,-20,3])
                    cube([16,30,7]);
                translate([-21.5,-20,3])
                    rotate([0,0,180])
                        arc(20,7);

            }

            // mask off
            translate([0,0,1.5])
            cube([80,80,3],center=true);
        }
    }
    
    
    // cylinder for left screw
    translate([-25,-2,9.5-1])
        difference() {    
        translate([0,0,0])
            rotate([0,-90,0]) 
                cylinder(d=cyl_outer,h=15);
        
        translate([-2,0,0])
            rotate([0,-90,0]) 
                cylinder(d=cyl_inner,h=15);
        translate([-15,-2.9,0])
            cube([5,cyl_inner-0.5,10]);

        }
    // mount for right screw
    difference() {
        translate([17.2,-33,11.2])
            cube([9,43,35]);
        translate([17.2,-20,46])
            rotate([0,90,0])
                cylinder(d=24,h=9);
        translate([17.2,-40,34])
            cube([9,20,14]);
    }
   
    // right screw
    screw_offs = 1;
    screw_dist = 26;
    translate([18,-2,9-1.5+screw_dist+screw_offs]) {
        difference() {    
            translate([0,0,0])
                rotate([0,-90,0]) 
                    cylinder(d=cyl_outer,h=15);
            
            translate([-2,0,0])
                rotate([0,-90,0]) 
                    cylinder(d=cyl_inner,h=15);
            translate([-17,0,-2.85])
                cube([15,10,cyl_inner-0.5]);
        }
    }
    
    // base mount w an oval hole
    translate([-13.73,-39,2.5+8]) {
        translate([0,0,0])
            rotate([90,0,180])
                arc(15,6.2);
        difference() {
            cube([39.9,6.2,23.5]);
            hull() {
                translate([10,0,10])
                   rotate([-90,0,0])
                        cylinder(d=4.2,h=10);
                translate([20,0,10])
                   rotate([-90,0,0])
                        cylinder(d=4.2,h=10);
            }
        }
    }
}



if (print)    
    translate([0,0,34]) rotate([0,90,0])new();
else {
    *rotate([0,0,180]) cube([60,60,37-cyl_inner/2]);
    translate([0,0,39]) rotate([90,0,0]) new();
}
    
