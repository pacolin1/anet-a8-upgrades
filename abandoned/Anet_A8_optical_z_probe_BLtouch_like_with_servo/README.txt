                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2805327
ANET A8 optical z probe (BLtouch like with servo) by aeropic is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

My big inductive probe suddenly died (led always ON) after only 6 months of good services... I ordered a new one on ebay from China, but as I woudd have to wait 4 weeks or so I decided to build an optical one which to my big surprize seems being really very accurate.

The idea is to make it as minimal as possible with just  a flag attached to a metal pin moving between the fork of an optical IR sensor. The design makes the pin fully free to move under gravity and a cheap RC servo motor will park and release the probe .

After few days operational,  I will for sure keep the optical one which gives several advantages:
- lighter (the inductive weights 80g)
- closer to the nozzle
- less intrusive, once retracted the pin is nearly at the level of the bottom of the heatblock which gives the opportunity to clean the nozzle easily
- very accurate
- it senses the top of the glass and not under the glass as inductive probes do
- it is cheap : you can get it for less than 3€ ...

I wanted to get the probe as close as possible to the hot end and succeeded to hide it on the back left corner just behind the left fan.

It is working very well, here is a repeatability test performed under MARLIN with M48 GCODE::
12:18:09.925 : M48 Z-Probe Repeatability Test
12:18:44.120 : Finished!
12:18:44.120 : Mean: -0.140000 Min: -0.141 Max: -0.137 Range: 0.005
12:18:44.120 : Standard Deviation: 0.001483

As you can see the range is 5 µm, and a standard deviation < 2µm which is better than the accuracy of the other parts of the printer ;-)

see it running here :https://youtu.be/GwbwwtUL2Po

EDIT : for those who like a cover, there is one !

# Print Settings

Printer: ANET A8
Rafts: No
Supports: No
Resolution: 0.2 mm
Infill: 20%

Notes: 
print all the pieces in PLA.
The flag must be printed in a dark color. Avoid the red color for it as the IR light will not be blocked by red filament...

# Post-Printing

## the support :

there is a little 1mm thick support to avoid too long bridge, just cut it once printed.
Then drill the holes of the support :
- 2,5 mm for the pin holes
- 4 mm for the screws

![Alt text](https://cdn.thingiverse.com/assets/bc/89/70/cd/cc/support.jpg)

## the optical sensor KTIR 0311S

The optical sensor I use is a KTIR 0311S. 
(Another device could be the TCST 2103 it comes with a support including 2 mounting holes, you might have to cut one support leg to avoid interferences with the X belt ...
You might as well order a cheap ready to use optical endstop, in that cas too , you will have to trim the length of the PCB to suppress one mounting leg. Those sensors are 0.3mm thicker than the KTIR, select the IR_sensor_support_TCST2103.stl which has a bigger hole)

I bought my KTIR 03311S fully bare (with no PCB). in that case you'll have to solder a 470 Ohm resistor between the two short legs.
The + of the IR led (transparent component close to the hole) shall be connected to +5V (middle pin of the Z endstop plug)
The free pin of the photo transistor (the long one) shall be directly connected to signal pin of the z endstop connector (the ANET board has active pullups on its entries)
The other pin of the transistor (the short one with the resistor) shall be connected to the ground (left pin of the Z endstop connector)

screw it in place (this is not shown in the picture bellow)

![Alt text](https://cdn.thingiverse.com/assets/58/32/22/d8/77/opto.jpg)
bare KTIR 0311S need a 470 Ohm resistor

![Alt text](https://cdn.thingiverse.com/assets/ed/88/96/c6/49/TCST.jpg)
TCST 2103 on PCB : just trim the PCB at the level of the green line !

## building the pin

cut a 12 cm long piece of steel rod. I made mine from a pressing coat hanger !
File the tips so that they are round with no sharp edges.

Nota : The 12 cm  length of the rod perfectly fits a standard ANET's hot end as much inserted as possible inside the extruder mount (the lip of the throat is nearly in contact with the brass gear). Should you prefer a bigger gap between the throat and the gear, just cut your rod 5mm longer !

Drill the hole of the top piece at 2 mm.
Then with a hammer insert the top piece on the rod with the flat surface on top.

Once done, insert the pin in the holes of the support and check it is straight and free to move.
Then, pull it and insert the flag as shown here after. The window of the flag shall be in the upper position.

Shift the flag on the rod so that,when the top piece is in contact with the support (rod being released by the servo) the IR switch would be ready to trigger with 2mm motion of the rod

![Alt text](https://cdn.thingiverse.com/assets/23/68/47/d0/4e/top_pin.jpg)

![Alt text](https://cdn.thingiverse.com/assets/5c/13/a5/e3/15/20180224_183450.jpg)

## the servo ...

It is a standard 9g microservo for arduino (find it on ebay for 1,5€ or so ...).
Stick it in place with some double sided tape and secure it with a tie rap (the servo axis shall be on the left).
I use a special printed arm although a standard one would maybe do the job ... Thanks for the library of  (c) 2012 Charles Rincheval. This curved servo arm allows to keep the arm under the plastic pad of the rod it prevents the arm to escape when the printer is powered ON, as the servo may have some jitter before reaching its zero position...

You'll get several heads diameter in the files section named "servo_arm_xx_yy.stl".
- xx is the diameter of the head
- yy is the extra diameter added so that the hole is slightly bigger than the head.

 If your servo is a standard micro servo for arduino try using  "servo_arm_48_02.stl". If too big then go for a smaller figure 48_01 for instance ...

## final wiring...

the servo shall be connected between GND, +5V, and pin 3 of LCD connector (as for BL touch snesors).
(If you follow the pin3 wire of the LCD connector, you'll see it goes to the LCD PCB where a soldering pad is available... This could be an option not to dismantle the ANET's board...)

you can get the 5v and ground for the servo straight off the back of the optical sensor, and then get the signal off the pad on the back of the LCD. This means a few less wires going back to your main board. 



![Alt text](https://cdn.thingiverse.com/assets/6d/ad/12/9f/9a/wiring.jpg)

![Alt text](https://cdn.thingiverse.com/assets/d4/43/03/59/5b/BLTouchAdapter.jpg)
If you don't feel comfortable soldering on the ANET board, just buy a BLTouch adapter

## firmware ...

on MARLIN just modify your configuration.h file with the following lines :
(I provide my configuration.h just for information, please take care as lots of functions are activated which makes the size of the binary too big to be uploaded to the ANET without flashing first the optiboot to recover memory... Use it to compare with yours only or know what you do !)

#note: I replace here after all # by '# in order to avoid thingiverse to use big bold character set :

// Mechanical endstop with COM to ground and NC to Signal uses "false" here (most common setup).
'#define X_MIN_ENDSTOP_INVERTING true  // set to true to invert the logic of the endstop.
'#define Y_MIN_ENDSTOP_INVERTING true  // set to true to invert the logic of the endstop.
'#define Z_MIN_ENDSTOP_INVERTING false  // set to true to invert the logic of the endstop.
'#define X_MAX_ENDSTOP_INVERTING false // set to true to invert the logic of the endstop.
'#define Y_MAX_ENDSTOP_INVERTING false // set to true to invert the logic of the endstop.
'#define Z_MAX_ENDSTOP_INVERTING false // set to true to invert the logic of the endstop.
'#define Z_MIN_PROBE_ENDSTOP_INVERTING false  // set to true to invert the logic of the probe.

'#define Z_MIN_PROBE_USES_Z_MIN_ENDSTOP_PIN

/**
 * Z Servo Probe, such as an endstop switch on a rotating arm.
 */
'#define Z_ENDSTOP_SERVO_NR 0   // Defaults to SERVO 0 connector.
'#define Z_SERVO_ANGLES {115,10}  // Z Servo Deploy and Stow angles

'#define X_PROBE_OFFSET_FROM_EXTRUDER -13   // X offset: -left  +right  [of the nozzle] 60 avec induct à froite
'#define Y_PROBE_OFFSET_FROM_EXTRUDER 22   // Y offset: -front +behind [the nozzle] 0 avec induct à froite
'#define Z_PROBE_OFFSET_FROM_EXTRUDER xxxxxxxxxx   // Z offset: -below +above  [the nozzle]  to be trimmed on your machine !!!!!!!!!!!!!!!

'#define Z_CLEARANCE_DEPLOY_PROBE  5 // Z Clearance for Deploy/Stow  
'#define Z_CLEARANCE_BETWEEN_PROBES  5 // Z Clearance between probe points 

then select the options yopu wish for autobed leveling (I use bilinear, 9 points)

don't forget this :
'#define Z_SAFE_HOMING

'#if ENABLED(Z_SAFE_HOMING)
 // #define Z_SAFE_HOMING_X_POINT ((X_BED_SIZE) / 2)    // X point for Z homing when homing all axes (G28).
//  #define Z_SAFE_HOMING_Y_POINT ((Y_BED_SIZE) / 2)    // Y point for Z homing when homing all axes (G28).
'#define Z_SAFE_HOMING_X_POINT (10)    // X point for Z homing when homing all axis (G28). 30 avec inductive à droite
 ' #define Z_SAFE_HOMING_Y_POINT (20)    // Y point for Z homing when homing all axis (G28).
'#endif

and at the end of the file :
/**
 * R/C SERVO support
 * Sponsored by TrinityLabs, Reworked by codexmas
 */

/**
 * Number of servos
 *
 * For some servo-related options NUM_SERVOS will be set automatically.
 * Set this manually if there are extra servos needing manual control.
 * Leave undefined or set to 0 to entirely disable the servo subsystem.
 */
'#define NUM_SERVOS 1 // Servo index starts with 0 for M280 command

// Delay (in milliseconds) before the next move will start, to give the servo time to reach its target angle.
// 300ms is a good value but you can try less delay.
// If the servo can't reach the requested position, increase it.
'#define SERVO_DELAY { 300 }

// Servo deactivation
//
// With this option servos are powered only during movement, then turned off to prevent jitter.
'#define DEACTIVATE_SERVOS_AFTER_MOVE
"

## final testing

When powering on the servo should move slightly (this is good sign).
to test the servo send this gcode:
M280 P0 S0       <======== move the servo to zero position (the arm shall be up)
M280 P0 S115   <======== move the servo to the down position, the arm is down the pin is touching the top support surface

First check you new Z endstop:
send a M119 ==> with the probe low you should get a OPEN status.
Manually rotate the servo to rise the pin by 5 mm, send another M119 you should get a TRIGGERED status.

You can slide the flag so that it acts correctly... Check that it triggers before the head touches the bed ...
Once in position glue the flag to the pin with a drop of CA glue...

Now you can do the end to end test:
- home all axis
- send G29 to probe the bed

This is the way you measure and set your Z offset
Heat your printer up to your printing temperature and allow a few minutes for it to expand and settle
Reset the existing Z-offset to zero
M851 Z0
Home all axes
G28
Move the nozzle to the middle
G1 X110 Y110
Turn off the software endstops with
M211 S0
Move the nozzle down so it is just barely touching the bed
Set the Z-offset to the displayed value. E.g. if the printer displays a Z-Value of -1.23 enter
M851 Z-1.23
Store it to the EEPROM
M500
Important Enable the endstops again with
M211 S1

## my other ANET A8 mods



If you like this mod for the ANET A8, you might be interested in my other ANET's designs all of them are here to improve the ANET A8 printer...

https://www.thingiverse.com/thing:2537701
https://www.thingiverse.com/thing:2456797
https://www.thingiverse.com/thing:2430113
https://www.thingiverse.com/thing:2426747
https://www.thingiverse.com/thing:2494319
https://www.thingiverse.com/thing:2498478
https://www.thingiverse.com/thing:2502858
https://www.thingiverse.com/thing:2527328
https://www.thingiverse.com/thing:2586970
https://www.thingiverse.com/thing:2597957
https://www.thingiverse.com/thing:2599816
https://www.thingiverse.com/thing:2640562
https://www.thingiverse.com/thing:2789046