 distance_trous = 18;

$fn = 100;

module flag(){
    difference () {
    union() {
    
    cylinder (d= 3, h = 34, center = true);
    translate([-3.5,-0.5,0]) cube ([10,2,34], center = true);  
    }
 
    cylinder (d= 2, h = 40, center = true);
    translate([-5,0,10]) cube ([6,4,10], center = true); 
   translate([0,2,0]) cube ([1.5,4,44], center = true);  
} // end diff
} //end flag

module top(){
    difference () {
        union() {
            cylinder (d= 5, h = 6, center = true);
            hull(){
                translate([0,2,-2.25]) cylinder (d=9,h=1.5, center = true); 
         #   translate([3,9.5,-2.25]) cylinder (d=3,h=1.5, center = true);
                #   translate([-3,9.5,-2.25]) cylinder (d=3,h=1.5, center = true);
               * translate([0,8,-2.25]) cube ([9,6,1.5], center = true); 
                
            } // end hull
            
        }
        cylinder (d= 2, h = 30, center = true); 
} // end diff
} //end top

module support_top() {
    difference () {
        union() {
       translate([6,0,0]) cube ([+5+10+ distance_trous+5,3,12], center = true); 
            # translate([3.25,-10,-7.]) cube ([+20.5+ distance_trous+5,23,2], center = true); // support servo
        translate([-11/2-6,0,+34.5/2-12/2])    cube ([14,3,35.5], center = true);
           translate([-5.75,-4.5,20+35/2-12/2])    cube ([2.5,6,40+35], center = true); 
        translate([-5,0,24])    cube ([21,3,10], center = true); // patte housing bas
            #  translate([11.1,0,14]) rotate([0,50,0])   cube ([30.5,3,10], center = true); // patte diagonale
        translate([-11.5,-3.5,0/2-12/2])    cube ([14,3+5,4], center = true); // support axe haut
            translate([-11.5,-4.5,40+27])    cube ([14,6,5], center = true); // support axe bas
          *  translate([-11.5,-4.5,40+10])    cube ([14,6,0.7], center = true); // support à casser
            translate([-11.5,-4.5,30+0])    cube ([14,6,1], center = true); // support à casser
            translate([0.5,-6.25,40+19.5])    cube ([15,2.5,20], center = true); // patte bas
        translate([-13.,4.5,24])    cube ([11,12,10], center = true); // support sonde
    } // end union
    // trous housing
  #translate([-5+(11+5+10+ distance_trous+5)/2,0,0])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    #translate([-5+(11+5+10- distance_trous+5)/2,0,0])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    #translate([-5+(11+5+10- distance_trous+5)/2,0,24])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    // trou flag
    #translate([-5-6-5+(11+5+10- distance_trous+5)/2,-4.0,0])  rotate([0,0,0]) cylinder (d= 2, h = 50, center = true);
    #translate([-5-6-5+(11+5+10- distance_trous+5)/2,-4.0,80])  rotate([0,0,0]) cylinder (d= 2, h = 50, center = true);
  # translate([-3-6.5,-4.0,13]) scale ([1,1,0.8])rotate([180,0,0])flag();
    
    // trou servo
    #translate([-0.5-6-5+(11+5+10- distance_trous+5)/2,-10.0,0])  rotate([0,0,0]) cylinder (d= 3, h = 50, center = true);
    #translate([23-0.5-6-5+(11+5+10- distance_trous+5)/2,-10.0,0])  rotate([0,0,0]) cylinder (d= 3, h = 50, center = true);
    
    // trou sonde
    translate([-14.75,+0.75,24])    cube ([7.51,4.51,6], center = true);
    #translate([-10,6.5,24])  rotate([0,90,0]) cylinder (d= 3.2, h = 20, center = true);
    #translate([-2,6.5,24])  rotate([0,90,0]) cylinder (d= 6.4, h = 15, center = true, $fn = 6);
    
    //biseau support haut
 #  rotate ([0,0,-45]) translate([-11.5,-24.2,5./2-12/2])    cube ([64,20,5], center = true); // support axe haut
    #  rotate ([0,0,-45]) translate([-15.5,-28,0./2-12/2])    cube ([64,20,5], center = true); // support axe haut
    } // end diff
} // end support_top

module support_top_TCST2103() {
    difference () {
        union() {
       translate([6,0,0]) cube ([+5+10+ distance_trous+5,3,12], center = true); 
            # translate([3.25,-10,-7.]) cube ([+20.5+ distance_trous+5,23,2], center = true); // support servo
        translate([-11/2-6,0,+34.5/2-12/2])    cube ([14,3,35.5], center = true);
           translate([-5.75,-4.5,20+35/2-12/2])    cube ([2.5,6,40+35], center = true); 
        translate([-5,0,24])    cube ([21,3,10], center = true); // patte housing bas
            #  translate([11.1,0,14]) rotate([0,50,0])   cube ([30.5,3,10], center = true); // patte diagonale
        translate([-11.5,-3.5,0/2-12/2])    cube ([14,3+5,4], center = true); // support axe haut
            translate([-11.5,-4.5,40+27])    cube ([14,6,5], center = true); // support axe bas
          *  translate([-11.5,-4.5,40+10])    cube ([14,6,0.7], center = true); // support à casser
            translate([-11.5,-4.5,30+0])    cube ([14,6,1], center = true); // support à casser
            translate([0.5,-6.25,40+19.5])    cube ([15,2.5,20], center = true); // patte bas
        translate([-13.,4.5,24])    cube ([11,12,10], center = true); // support sonde
    } // end union
    // trous housing
  #translate([-5+(11+5+10+ distance_trous+5)/2,0,0])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    #translate([-5+(11+5+10- distance_trous+5)/2,0,0])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    #translate([-5+(11+5+10- distance_trous+5)/2,0,24])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    // trou flag
    #translate([-5-6-5+(11+5+10- distance_trous+5)/2,-4.0,0])  rotate([0,0,0]) cylinder (d= 2, h = 50, center = true);
    #translate([-5-6-5+(11+5+10- distance_trous+5)/2,-4.0,80])  rotate([0,0,0]) cylinder (d= 2, h = 50, center = true);
  # translate([-3-6.5,-4.0,13]) scale ([1,1,0.8])rotate([180,0,0])flag();
    
    // trou servo
    #translate([-0.5-6-5+(11+5+10- distance_trous+5)/2,-10.0,0])  rotate([0,0,0]) cylinder (d= 3, h = 50, center = true);
    #translate([23-0.5-6-5+(11+5+10- distance_trous+5)/2,-10.0,0])  rotate([0,0,0]) cylinder (d= 3, h = 50, center = true);
    
    // trou sonde
   # translate([-14.75,+0.75,24])    cube ([7.51,4.51,6.35], center = true);
    #translate([-10,6.5,24])  rotate([0,90,0]) cylinder (d= 3.2, h = 20, center = true);
    #translate([-2,6.5,24])  rotate([0,90,0]) cylinder (d= 6.4, h = 15, center = true, $fn = 6);
    
    //biseau support haut
 #  rotate ([0,0,-45]) translate([-11.5,-24.2,5./2-12/2])    cube ([64,20,5], center = true); // support axe haut
    #  rotate ([0,0,-45]) translate([-15.5,-28,0./2-12/2])    cube ([64,20,5], center = true); // support axe haut
    } // end diff
} // end support_top


module head_cover() {
    difference() {
        union() {
        translate([6,2.5,-1]) cube ([+5+10+ distance_trous+5,2,14], center = true); 
    translate([-3.5,1,-12])cube([30,17,36], center = true);
    } // end union
   # translate([-3.5,-3.5,1])cube([34,10,18], center = true);
   # translate([-3.5,-1.5,-10.5])cube([27,19,36], center = true);
   # translate([10,-6,-10.5])cube([4,12,36], center = true);
    // trous housing
  #translate([-5+(11+5+10+ distance_trous+5)/2,0,0])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    #translate([-5+(11+5+10- distance_trous+5)/2,0,0])  rotate([90,00,0]) cylinder (d= 4, h = 10, center = true);
    //anet logo
    #  translate([-18,8,-24]) scale([0.2,1,0.2]) rotate([-90,0,0])import("Anet_Logo.stl");
   # translate([-7.6,0,-19.3]) rotate ([90,-49,-0])scale([0.7,1.5,1]) cylinder(d =3, h =20, center = true); // trou du A
    # translate([2.8,0,-17]) rotate ([90,0,-0])scale([2.7,1,1]) cylinder(d =1.6, h =20, center = true); // trou du E
} // end dif
} //end head cover

* rotate ([90,0,-0])  flag();
*rotate ([0,-90,-0]) support_top_TCST2103();
*top();

rotate ([-90,-0,-0]) head_cover();

