scale([1,1,1/4.5])
{
rotate([90,0,0])
import("A8-0005.stl");
linear_extrude(4.5, convexity=10)
	polygon([[10,1],[10,40],[70,40],[70,1]]);
}