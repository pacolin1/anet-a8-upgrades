//translate([0,0,28])
//rotate([180,0,180])
//color("gray",.25)
//	import("filamentSensorHoleX-Carriage.stl");

rotate([0,-90,0])
linear_extrude(2)
difference()
{
	polygon([[0,0],[33,0],[33,27],[0,27]]);
	translate([27.5,5.5])
		circle(d=3.4, $fn=24);
	translate([27.5,19.5])
		circle(d=3.4, $fn=24);
}

translate([0,27,0])
rotate([0,-90,90])
linear_extrude(2)
  polygon([[0,0],[33,0],[28,14],[0,14]]);
	
translate([-12,25,0])
rotate([0,-90,0])
linear_extrude(2)	
difference()
{
	polygon([[0,0],[28,0],[28,20],[0,20]]);
	translate([5,15.4])
		circle(d=4.3, $fn=24);
	translate([23.4,15.4])
		circle(d=4.3, $fn=24);
}


linear_extrude(23)
polygon([[-1,26],[-11,26],[-1,20]]);

linear_extrude(28)
polygon([[-13,26],[-3,26],[-13,32]]);