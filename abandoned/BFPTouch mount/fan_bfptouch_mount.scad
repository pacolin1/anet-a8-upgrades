difference()
{
    rotate([0,0,90])
    translate([241.5,-197,28])
        import("Anet_Fan_Sensor_Holder_BLT_Leo_N.stl");
    translate([-15,-17.5,0])
        cube([30,30,50]);
    translate([-8.85,20,28])
        cube([2,30,10]);
}

// translate([0,0,48.3])
// rotate([180,0,-90])
//     import("BLTouch_extracted.stl");
// //    import("BLTouch_contracted.stl");

// translate([0,7,2])
// rotate([90,0,0])
//     import("bfptouch.stl");

translate([0,0,28])
linear_extrude(18, convexity=10)
    polygon([[-8,6.1],[-6.85,13],[12,13],[8.5,6.1]]);

// translate([-7,6.1,51])
// rotate([0,90,0])
// linear_extrude(10, convexity=10)
//     polygon([[0,0],[5,5],[5,0]]);

translate([0,0,46])
linear_extrude(5, convexity=10)
    polygon([[-8,6.1],[-7.2,11.1],[11.1,11.1],[8.5,6.1]]);

difference()
{
    translate([0,0,48.6])
    linear_extrude(2.4, convexity=10)
        polygon([[-12.2,-6.1],[-12.2,1.9],[-8,6.1],[8.5,6.1],[12.2,1.9],[12.2,-6.1]]);

    translate([9,0,47])
        cylinder(d=3.4, h=10, $fn=30);
    translate([-9,0,47])
        cylinder(d=3.4, h=10, $fn=30);
}

translate([-3,11,51])
rotate([90,0,0])
linear_extrude(17, convexity=10)
    polygon([[-2,0],[0,2],[2,0]]);

translate([3,11,51])
rotate([90,0,0])
linear_extrude(17, convexity=10)
    polygon([[-2,0],[0,2],[2,0]]);