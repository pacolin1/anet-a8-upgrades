// for reference, the A8's frame component that holds the control panel, and the sensor
// (uncomment to view)
 // color([.75,.75,.75])
 // {
     // rotate([0,0,0])
     // translate([0,0,80])
     // import("A8-0011.stl");
 
     // rotate([0,-90,180])
     // translate([88.3,48,-1.8])
     // import("sensor.stl");
 // }

// // over the front of the control panel

difference()
{
    translate([0,-.1,0])
    rotate([90,0,0])
    linear_extrude(4)
    {
        difference()
        {
            union()
            {
                difference()
                {
                    // basic truss-like shape with extensions for the screw holes
					union()
					{
						polygon([//[-83,56.5],
								[-83,61.5],
								[-83,70.5],
								[-6.5,84.1],
								[2.5,84.1],
								[78,70.5],
								[78,61.5]
								//[78,56.5],
								//[70,56.5],
								//[70,61.5],
								//[-75,61.5],
								//[-75,56.5]
								]);
						polygon([[-83,61.5],
								 [-83,78.9],
								 [-75,78.9],
								 [-75,61.5]]);
						polygon([[70,61.5],
								 [70,78.9],
								 [78,78.9],
								 [78,61.5]]);
					}
                    // cut out the middle
                    polygon([[-75,65.5],
                            [-75,68.5],
                            [-2.5,80.1],
                            [70,68.5],
                            [70,65.5]]);
                }
                // make a loop above the button for finger clearance
                difference()
                {
                    translate([52.5,57,0])
                        circle(d=26, $fn=360);
                    translate([52.5,57,0])
                        circle(d=19, $fn=360);
                }
                polygon([[-3.75,64.5],
                        [-3.75,83],
                        [.25,83],
                        [.25,64.5]]);
            }
        translate([52.5,57,0])
            circle(d=19, $fn=360);
        polygon([[70,61.5],
                [35,61.5],
                [35,31.5],
                [70,31.5]]);
        // drill screw holes
        translate([-79,/*61.5*/70.2,0])
            circle(d=3.5, $fn=360);
        translate([74,/*61.5*/70.2,0])
            circle(d=3.5, $fn=360);
        }
    }

    // countersink drill holes
    translate([74,-1.61,/*61.5*/70.2])
    rotate([90,0,0])
    linear_extrude(5)
        circle(d=5.75, $fn=360);
    translate([-79,-1.61,/*61.5*/70.2])
    rotate([90,0,0])
    linear_extrude(5)
        circle(d=5.75, $fn=360);
}

// on top of the control panel

translate([0,0,80.1])
linear_extrude(4)
{
    polygon([[-6.5,-3.1],[2.5,-3.1],[2.5,10],[-6.5,10]]);
}

// behind the control panel

translate([0,12.5,0])
rotate([90,0,0])
linear_extrude(4)
    polygon([[-6.5,84.1],[2.5,84.1],[2.5,75],[-6.5,75]]);

// sensor support extension

translate([-3.75,-30,80.1])
linear_extrude(4)
    polygon([[0,0],[0,30],[4,30],[4,0]]);

// vertical brace

rotate([-45,0,0])
translate([-3.75,-73.7,40.6])
linear_extrude(4)
    polygon([[0,0],[0,27.4],[4,27.4],[4,0]]);

// left horizontal-ish brace
lb_points=[
    [-39.75,22.5,-7.15], // bottom front left
    [4,0,0], // bottom front right
    [4,5.75,0], // bottom rear right
    [-30,22.5,-5.6], // bottom rear left
    [-39.75,22.5,-3.35], // top front left
    [4,0,4], // top front right
    [4,5.75,4], // top rear right
    [-30,22.5,-1.6]  // top rear left
];

lb_faces=[
    [0,1,2,3], // bottom
    [4,5,1,0], // front
    [7,6,5,4], // top
    [5,6,2,1], // right
    [6,7,3,2], // back
    [7,4,0,3]  // left
];

translate([-7.74,-22.7,80.1])
polyhedron(points=lb_points, faces=lb_faces);

// right horizontal-ish brace

mirror([1,0,0])
translate([-3.74,-22.7,80.1])
polyhedron(points=lb_points, faces=lb_faces);
