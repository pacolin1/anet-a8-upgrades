r=1.5; // ring cross-sectional radius
r2=1/4*25.4; // ring centerline radius
a=340; // ring angle (360 for no gap)
h=4.65; // hitch inner dimension 
ht=0.8; // hitch thickness

// the ring

rotate([0,0,(360-a)/2])
rotate_extrude(angle=a, $fn=90)
translate([r2,0,0])
circle(d=2*r, $fn=90);

// the hitch

difference()
{
	translate([-18,0,(h+2*ht)/2-r])
	rotate([0,-90,0])
	rotate([0,0,45])
	linear_extrude(12)
		circle(d=sqrt(2*(h+2*ht)*(h+2*ht)), $fn=4);

	translate([-21,0,(h+2*ht)/2-r])
	rotate([0,-90,0])
	rotate([0,0,45])
	linear_extrude(9)
		circle(d=sqrt(2*h*h), $fn=4);
}

// the connector between them

pts=[[-18,-(h+2*ht)/2,-r],
	 [-r2,-r,-r],
	 [-r2,r,-r],
	 [-18,(h+2*ht)/2,-r],
	 [-18,-(h+2*ht)/2,h+2*ht-r],
	 [-r2,-r,r],
	 [-r2,r,r],
	 [-18,(h+2*ht)/2,h+2*ht-r]];

faces = [
  [0,1,2,3],  // bottom
  [4,5,1,0],  // front
  [7,6,5,4],  // top
  [5,6,2,1],  // right
  [6,7,3,2],  // back
  [7,4,0,3]]; // left

polyhedron(pts, faces);