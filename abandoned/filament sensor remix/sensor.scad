difference()
{
    union()
    {
        // add onto this
        // (from https://www.thingiverse.com/thing:2010235)
        import("switchbox-remix-main.stl");

        // a mount for the frame
        hs=4.65; // hole size
        translate([-6.2,-17.8,0])
        rotate([90,0,0])
        linear_extrude(7)
        difference()
        {
            polygon([[-(hs+2)/2,-4.85],
                    [-(hs+2)/2, 4.85],
                    [ (hs+2)/2, 4.85],
                    [ (hs+2)/2,-4.85]]);
                rotate([0,0,45])
                        circle(d=sqrt(2*hs*hs), $fn=4);
        }

        // a filament cleaner holder
        translate([13,15.25,0])
        rotate([0,90,0])
        linear_extrude(12)
        difference()
        {
            polygon([[-4.85,-4.85],
                    [-4.85, 4.85],
                    [ 4.85, 4.85],
                    [ 4.85,-4.85]]);
            polygon([[-4.1,-4.1],
                    [-4.1, 4.1],
                    [ 4.1, 4.1],
                    [ 4.1,-4.1]]);
        }
        translate([11.4,14.63,0])
        rotate([90,0,105])
        linear_extrude(3)
        polygon([[ 4.85, 4.85],
                [ 4.85,-4.85],
                [ 3.55,-4.85],
                [ 3.55, 4.85]]);
    }
    translate([10.2,17.6,3.9])
    linear_extrude(1)
    polygon([[0,0],
             [2,0],
             [2,1],
             [0,1]]);

    // drill a hole for a setscrew
    color([.75,.75,.75])
    rotate([0,0,0])
    translate([-6.2,-21.4,-5])
    linear_extrude(5)
    circle(d=1/16*25.4, $fn=360);
}
