                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2121279
"Mistral 2.1" Extruder Cooling Duct for the Anet A8 Printer by Leo_N is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

I've been designing and printing all kinds of cooling ducts for the last 2 months as I just wasn't happy with what I had. Even the popular round ducted coolers just don't do a good job in directly cooling the filament whilst keeping the heat block hot. Also many found on the internet are too clunky, too heavy or are IMHO just terribly designed.
With the new Marlin and other 3D printer firmware's incorporating thermal protection mechanism it now becomes even more important to have a design that will not cause false alarms. This is particularly the case when the cooling ducts blow air on the heat block and not below the nozzle onto the filament. 
That is why I created "Mistral" : http://www.thingiverse.com/thing:2086191
Mistral serves 90% of all objectives needed to get good printing results when it comes to cooling. One area it lacks however is cooling on rear overhangs because not enough cooling air could reach the filament sufficiently and in time.

That is why after 2 months development and countless prints and tests I designed "Mistral 2.x".
It uses two ducts set at 90° for the air reach the backside of the printing object.
90° was chosen to dissipate the heat as quickly as possible and to ensure that the air would not collide (e.g. a problem with 120° fan ducts) but still ensure constant air flow from front to rear over the whole nozzle end area.

<B>Design objectives:</B>
- Blow high volume air on the filament just beyond the nozzle.
- Avoid getting any cooling air on the heater block.
- Designed to get the hot air away from the object as fast as possible.
- Create a design that will not disrupt the view surrounding the nozzle.
- Make it easy to get to the nozzle with tweezers to remove filament. 
- Create a version that is compact and will not interfere with auto level sensors. 
- Reduce the noise level of the fan as much as possible.
- Make it as light as possible to avoid high mass on the extruder. Version 2.1 (PLA) and 2.1a (ABS) weigh ca. 4.4 grams.

<B>Attention:</B>
- This cooling duct is intended for use with the standard unmodified A8 printer.
- It is recommended but not mandatory that the cooler be printed in ABS or PETG as these filaments are more heat resistant. However I have mine printed in PLA without any issues. When printing ABS or high temp filaments I just remove the cooler. You normally don't print with the fan on for high temp filaments.
- With this cooler you might need to reduce airflow with some prints! Sometimes I have to print with 75% or less where in the past air flow was set to 100%.
- This object must be printed up-right (see picture).
- When mounting the fan back to the extruder bracket make sure it is at it's lowest position before tightening the two screws!
- I recommend you use version "a" as it provides slightly more airflow and it will not come loose from the fan.
- Please pay close attention to the pictures so the position of the cooler is clear.
- Make sure your nozzle is centered between the two fan ducts to possibly avoid thermal runaways!
 
I want to thank Jan Hedström for testing this cooling duct and confirming the effectiveness.
Also thanks for the idea of giving it a cool name "Mistral"!

Please let me know if you have a problem with the design so I can improve it if necessary.

Also please post a picture with comments if you have made one.

Thanks.

Leo
_______________________________________________________________________

If you are using Marlin then you can use a function to calibrate your extruder nozzle via „PID Autotuning“.
You will already find PID values in the firmware however it is a good idea to set them according to your surroundings.

This is how to calibrate the hotend:

First we need to get the PID values from the hotend. The values are read in 8 cycles using the following code:

M106
M303 E-0 S210 C8

Code description:
- M106 run nozzle at 100%
- M303 start PID autotuning
- E-0 select the first hotend
- S210 set extruder temperature. Here we chose 210°C
- C8 denotes the number of cycles. In this example 8

After the 8 cycles have completed you will see something like this (My values using Mistral 2.1a-4.5):

Kp 12.32
Ki 0.57
Kd 66.38

Now you will need to save the values to the printers eeprom:

M301 P12.32 I0.57 D66.38
M500

Code description:
- M301 temporary sets the PID values
- P = Kp
- I = Ki
- D = Kd
- M500 will save the temporary PID values to eeprom.

More information can be found here: http://reprap.org/wiki/PID_Tuning
______________________________________________________________
I found this on YouTube and thought it's kinda cool. I don't know how accurate the simulation is particularly showing the air flow after exiting the nozzles.
https://www.youtube.com/watch?v=Gy7fxzy1cBk
______________________________________________________________

<B>Update 2017.02.21</B>
Uploaded Mistral 2 "4.5" to support the "new" GearBest auto leveling sensor which is mounted between extruder holder and cooling fan.

<B>Update 2017.02.24</B>
Uploaded Mistral 2 "a" (revised 2017.02.26). This version is placed over the fan outlet instead of inserting it inside the outlet. This method of fixture was needed because many users, including myself, have damaged the fan hinges to a point that sticking the duct inside the fan would no longer keep it in place very securely with the chances of it even fall off.

<B>Update 2017.03.04</B>
Uploaded Mistral 2.1 and 2.1a. These two versions now incorporate the advantages that comes with Mistral 1.

<B>Update 2017.03.11</B>
Uploaded Mistral 2.1a-4.5. This is the newest version to accommodate Gearbest auto leveling sensor or http://www.thingiverse.com/thing:2188477 which is mounted between extruder holder and cooling fan.
You will need to remove the small support I added in the opening for the fan hinges.

<B>Update 2017.03.18</B>
Removed file "Anet_A8_Mistral_V2.0-4.5_Leo_N.stl".

<B>Update 2017.04.02</B>
Uploaded Mistral 2.1_10a. Designed to be used with https://www.thingiverse.com/thing:2169370 in conduction with a E3D Volcano.

<B>Update 2017.04.29</B>
"Mistral_V2.1....." files where updated. Air distribution now 50/50. Added name "Mistral". Version 2.1a is printed without support! Small tab in the slot needs to be removed before installment.

<B>Update 2019.01.13</B>
I've added the Mistral V2.1a to the files section in step format for download.


# Print Settings

Printer: Anet A8
Rafts: Doesn't Matter
Supports: Yes
Resolution: 0.2
Infill: 100%

Notes: 
Top and bottom layer height should be set to 0.8mm.
Wall thickness must must be set to 0.8mm!

Mistral 2.1:
Only use support for the portion of the throat that goes into the fan and build plate!
See orientation picture.

Mistral 2.1a and 2.0a-4.5 is printed without support. However don't forget to remove the small support wall that has been placed in the slot.