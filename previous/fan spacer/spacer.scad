// extruder fan spacer plate
//
// Scott Alfter
// 22 Mar 18

linear_extrude(3) // set thickness here
{
    difference()
    {
        union()
        {
            translate([6.5,6.5,0])
                circle(d=9, $fs=0.01);
            translate([38,6.5,0])
                circle(d=9, $fs=0.01);
            polygon([[6.5,2],[38,2],[38,11],[6.5,11]]);
        }
        translate([6.5,6.5,0])
            circle(d=3.4, $fs=0.01);
        translate([38,6.5,0])
            circle(d=3.4, $fs=0.01);
    }
}
