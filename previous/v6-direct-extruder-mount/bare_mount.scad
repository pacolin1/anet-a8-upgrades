mockup();   
//bare_mount();

module mockup()
{
    translate([-31,21,13.5])
    {
        rotate([90,0,0])
            %import("../e3d-v6-carriage-mods/E3DV6_BasePlate_V4_fixed.stl");
        translate([69,-24,27.5])
        rotate([0,180,0])
            %import("../e3d-v6-carriage-mods/bltouch-mount.stl");
    }

    translate([3.4,5.1,43.3])
    rotate([0,0,90])
        %import("../fan_duct_3/E3D_v6_To_Spec.stl");
}

module bare_mount()
{
    difference()
    {
        translate([-95,-35,161])
        rotate([-90,0,0])
            import("../e3d-v6-carriage/E3DV6_E3DHolder_without_Chain_V4.stl");

        translate([0,-35,9.1])
            cube([55,10,40]);
        translate([-10,-40,61.3])
            cube([50,50,30]);
        translate([21,-38,40])
            cube([30,10,30]);
    }

}

// filament path
//cylinder(d=1.75, h=300, $fn=20);

import("bare_mount.stl");

translate([-107.25,-8.5,-10])
rotate([0,-90,-90])
{
    import("Bowden_Motor_NoMount_fixed.stl");

    translate([81,107.25,8.5]) // Z,X,Y
    rotate([0,90,0])
    difference()
    {
        cylinder(d=10, h=6, $fn=45);
        cylinder(d=2, h=7, $fn=45);
    }
}