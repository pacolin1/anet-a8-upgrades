// import printer panel for reference

// translate([0, 0, -7.7])
// rotate([90, 0, -90])
// color([.75, .75, .75])
// import("A8-0004.stl");

// back hole: 19, 44.7
// right hole: 54.4, -4.5

// bottom
linear_extrude(2)
difference()
{
	polygon([[13.25,-10.25],
			 [13.25, 50.45],
			 [24.75, 50.45],
			 [60.15, 1.25],
			 [60.15, -10.25]]);
	polygon([[24.75, 1.25],
			 [24.75, 38.95],
			 [48.65, 1.25]]);
	translate([19, 44.7, 0])
		circle(d=3.5, $fn=45);
	translate([54.4, -4.5, 0])
		circle(d=3.5, $fn=45);
}

// side
translate([15.25, 0, -11.25])
rotate([0, -90, 0])
linear_extrude(2)
difference()
{
	polygon([[13.25, -10.25],
			 [13.25, 50.45],
			 [38.25, -8.25],
			 [38.25, -10.25]]);
	polygon([[18.25, -5.25],
			 [18.25, 27.45],
			 [31.25, -5.25]]);
}

// front
translate([0, -8.25, 12.25])
rotate([90, 0, 0])
linear_extrude(2)
difference()
{
	polygon([[13.25, -10.25],
			 [13.25, 14.75],
			 [60.15, 14.75],
			 [60.15, -10.25]]);
	polygon([[38.25, 14.75],
			 [60.15, 14.75],
			 [60.15, -10.25]]);
	translate([25.75, 2.25, 0]) // this positions the switch hole
		circle(d=6.35, $fn=45); // this drills it (1/4"=6.35 mm)
}