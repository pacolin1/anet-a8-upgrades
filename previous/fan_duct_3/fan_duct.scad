mockup();   
//old_duct();
circular_duct();

module mockup()
{
    translate([-31,21,13.5])
    {
        rotate([90,0,0])
            %import("../e3d-v6-carriage/E3DV6_BasePlate_V4_fixed.stl");
        translate([69,-24,26])
        rotate([0,180,0])
            %import("../alternate-bltouch-mount-2/mount.stl");
        translate([51.5,-56,35.5])
        rotate([0,0,180])
            %import("../e3d-v6-carriage/E3DV6_E3DHolder_with_Chain_No_Blower_chain_mod.stl");
    }

    translate([3.4,5.1,43.3])
    rotate([0,0,90])
        %import("E3D_v6_To_Spec.stl");
}

module old_duct()
{
    translate([-5,-35,25])
    rotate([90,0,0])
    color([.2,.2,.2,.5])
        %import("50mm_Fan.stl");

    translate([-3.9,-49.2,24.9])
    translate([113.9,134.7,-22.9])
    rotate([0,0,180])
        %import("../fan_duct/Shorter_Semi-Circular_Anet_A8_Fan_Duct.stl");
}

// section to be inserted into blower: 16.6x13.1x7

module circular_duct()
{
    difference()
    {
        union()
        {
            // mounting tab

            difference()
            {
                union()
                {
                    translate([-31,12,9.5])
                        cube([15,4,26]);
                    translate([-31,10,9.5])
                        cube([15,4,6]);
                }
                translate([-26,0,20.5])
                rotate([-90,0,0])
                    cylinder(d=3.4, $fn=20, h=30);
                translate([-21,0,31])
                rotate([-90,0,0])
                    cylinder(d=3.4, $fn=20, h=30);
            }

            // duct body

            difference()
            {
                translate([0,-5,1.5])
                {
                    difference()
                    {
                        rotate_extrude(angle=360, $fn=90)
                        translate([20,0])
                        {
                            intersection()
                            {
                                difference()
                                {
                                    circle(d=8, $fn=30);
                                    circle(d=6.4, $fn=30);
                                }
                                translate([-4,0])
                                    square(8);
                            }
                            translate([-4,0])
                                square([8,.8]);
                        }
                        for (i=[0:15:345])
                        {
                            rotate([0,0,i])
                            translate([0,0,-.5])
                            rotate([85.75,0,0])
                                cylinder(d=2.25,h=20,$fn=20);
                        }
                    }
                }
                translate([-25,-10,2.3])
                    cube([7,16.1,10]);
            }

            // duct inlet

            translate([-22,-10,12])
            rotate([0,0,90])
            {
                translate([18,-1,0])
                rotate([90,0,180])
                color([.2,.2,.2,.5])
                    %import("50mm_Fan.stl");

                difference()
                {
                    cube([16.6,13.1,7]);
                    translate([1.2,1.2,0])
                        cube([14.2,10.7,7]);
                }
            }

            translate([-22.6,-10,13])
            rotate([0,147,0])
                cube([0.8,16.1,9.75]);

            translate([-30,-10,1.5])
            cube([10,16.1,.8]);

            translate([-34.43,-10,12.5])
            rotate([0,154.2,0])
                cube([0.8,16.1,11.83]);

            translate([0,-9.2,0])
            rotate([90,0,0])
            linear_extrude(0.8, convexity=10)
                polygon([[-30,1.5],[-21,1.5],[-17.5,4.5],[-22,12],[-35,12]]);

            translate([0,6.6,0])
            rotate([90,0,0])
            linear_extrude(0.8, convexity=10)
                polygon([[-30,1.5],[-21,1.5],[-17,4.5],[-22,12],[-35.1,12]]);

            translate([-35.1,8.1,8])
                cube([5.05,2,9.5]);
            translate([-27.05,8.1,8])
                cube([5.05,2,9.5]);

            translate([-33,6.5,8])
                cube([11,2,4]);
        }

        // clean out duct interior
        translate([0,-5,2.3])
        {
            rotate_extrude(angle=360, $fn=90)
            translate([20,-0.8])
            intersection()
            {
                circle(d=6.4, $fn=30);          
                translate([-3.2,0.8])
                    square(6.4);
            }
        }
    }
}

