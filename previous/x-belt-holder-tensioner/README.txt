                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2574522
Anet A8 X Axis Tensioner by FredGenius is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

This is an improved X axis tensioner and belt gripper, simply bolts onto the back of the x axis carriage using the existing holes. 

The belt fits into the gripper slots, then you just tighten the slider screw to tension the belt. Simple.

You will need: 
4x M3x18mm screws (use the two that are currently holding the belt, should be a few more left over from the build).
1x M4x30mm screw.
1x M4 half nut.

# Print Settings

Printer: Anet A8
Rafts: No
Supports: No
Resolution: 0.2
Infill: 50%

Notes: 
Print the main body with the longest face on the bed. The gripper slots should be uppermost.

Print the slider on end, so the end with the slot and screw hole is facing up.

I used PLA for this, but if you want to use ABS then you have to allow for shrinkage. 

It is very important that the belt is a good fit in the gripper slots. Test by inserting the end of the belt (a scrap piece will do) and pulling hard. If you need to adjust the gripper size, use the 'Horizontal Expansion' setting in Cura. +-0.1mm might be necessary.

Insert the half-nut into the slot, fit one end of the belt into the gripper slot, screw the main body onto the back of the carriage.

Fit the other end of the belt into the slider's gripper, insert the screw and test-fit the slider in the main body's dovetail slot You will probably need to trim the belt until the slider just reaches the main body. Then, when you tighten the M4 screw all the way the tension will be perfect.



# More A8 Upgrades

You can find all my Anet A8 upgrades here:
https://www.thingiverse.com/FredGenius/collections/anet-a8-upgrades

I also highly recommend this Front Frame Brace by Leo Nutz: 
https://www.thingiverse.com/thing:1857991