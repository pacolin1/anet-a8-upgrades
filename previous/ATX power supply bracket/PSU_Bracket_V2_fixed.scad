difference()
{
	rotate([90,0,0])
		import("PSU_Bracket_V2.stl");

	translate([0,0,-.1])
	linear_extrude(3.4, convexity=10)
		polygon([[60,-14],[60,-60],[8,-60],[8,-14]]);

	translate([0,-14,3])
	rotate([90,0,0])
	linear_extrude(46,convexity=10)
		polygon([[8,0],[14,8],[14,0]]);
}

linear_extrude(3.1, convexity=10)
{
	polygon([[60,-11],[60,-63],[63,-63],[63,-11]]);
	polygon([[9,-60],[60,-60],[60,-63],[9,-63]]);
	polygon([[9,-11],[60,-11],[60,-14],[9,-14]]);
}

