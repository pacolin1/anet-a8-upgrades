import("RAMPS_1.4_Fan_HolderV2.stl");

$fn=60;

module spacer_1()
{
	translate([0,0,-.1])
	linear_extrude(12.8, convexity=10)
	difference()
	{
		circle(d=6.0);
		circle(d=3.0);
	}
}

module spacer_2()
{
	translate([0,0,-.1])
	linear_extrude(12.8, convexity=10)
	difference()
	{
		circle(d=5.8);
		circle(d=3.0);
	}
}

translate([6,6,15])
	spacer_1();
	
translate([88.2,6,28])
	spacer_2();

translate([88.2,54,28])
	spacer_2();
