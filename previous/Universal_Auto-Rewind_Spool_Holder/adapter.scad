// mockup
translate([21,0,0])
{
    translate([9.5,0,0])
    rotate([-90,0,90])
        %import("SpringRound_Stiff_v3.3.STL");

    translate([12,0,0])
    rotate([120,0,0])
    rotate([90,0,90])
        %import("Clutch_v3.3.STL");

    rotate([-23,0,0])
    rotate([90,0,90])
    translate([-53.5,-53.5,0])
        %import("Dial_v3.3.STL");

    translate([12,0,0])
    rotate([29,0,0])
    rotate([90,0,90])
    translate([-53,-53,0])
        %import("Hub_v3.3.STL");

    translate([-45,0,0])
    rotate([0,90,0])
        %cylinder(d=8, $fn=30, h=160);

    translate([-17.7,17.1,-112.5])
        %import("../../current/frame spoolholder/A8_Spool_Holder.stl");
}


difference()
{
    translate([4,0,-3])
        cube([34.3,38.3,30.3], center=true);
    translate([0,0,-3])
        cube([30.3,30.3,30.3], center=true);
    rotate([0,90,0])
        cylinder(d=8.5, $fn=30, h=30);
    translate([18,0,-4])
        cube([6.5,8.5,7], center=true);
}

