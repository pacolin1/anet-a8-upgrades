                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3338467
Universal Auto-Rewind Spool Holder by VincentGroenhuis is licensed under the Creative Commons - Attribution - Share Alike license.
http://creativecommons.org/licenses/by-sa/3.0/

# Summary

Filament load/unload issues? MMU2 taking too much space? Not with this Universal Auto-Rewind spool holder! It keeps filament on the spool during unloading, up to four full revolutions. [Video compilation](https://youtu.be/03iUbhrU8xw)

This is a remake of the Original and Parametric Auto-Rewind Spool Holder, but with significant enhancements:

* Slipping point of the clutch can be adjusted in about 15 steps using the dial.
* One size fits almost all spools up to 1kg. Inner diameter 30-75 mm, max outer diameter 200 mm, max width 88 mm. Hubs for wider spools can be created with the customizer, same for hubs with narrow diameters. Stand for 3kg spools available [here](https://www.thingiverse.com/thing:3369189).
* Spring (v1 and v1.01) works both ways around: towards the axle and towards the outer rim. Note: flipping the direction requires re-calibration of the clutch tension!
* Spring v1.02+ is direction independent, can be flipped around without changes in clutch tension. This version might also be better protected against spring overloading.

GCode for Prusa i3 MK2/S and MK3 /MM can be downloaded here: [single color](https://www.prusaprinters.org/prints/171-universal-auto-rewind-spool-holder-single-color) and [multi-material](https://www.prusaprinters.org/prints/168-universal-auto-rewind-spool-holder-mm).

The MM version uses two colors for most parts (three for the stand). First color is the base color (silver grey), second color is for decals and markers (vibrant color), third color is for the characters on the stand (white). The parts are designed such that missing layers in the 2nd and 3rd color do not affect the structural strength of all parts, also the number of color changes is kept to a minimum.

Four spring styles are available: SpringFlat is to be used with a 3-D printed axle with flat side. SpringRound (available in zip file) is used with a round axle (e.g. metal rod). SpringRound has an anti-rotation block which aligns with the frame, so it is not needed to immobilize the round axle. Both SpringFlat and SpringRound come in two different stiffnesses. The soft version is recommended to start with and the stiff version can be used when more torque is needed.

The hub thread length can be customized with the [Parametric](https://www.thingiverse.com/thing:3252111) Auto-Rewind Spool Holder customizer. Use output="hub", hubThreaded="yes", set hubThreadLength to the desired length and leave all other parameters at its default settings.

At Robotics and Mechatronics (RaM), University of Twente some research is done on variable stiffness actuators. Certain mechanisms in this Auto-Rewind Spool Holder were (loosely) inspired by our research there. One side of the stand reads "It's Rewind Time" while the other side reads "University of Twente". A stand with no text (blank) is also available.

A list of (mostly) compatible stands are on the [Parametric](https://www.thingiverse.com/thing:3252111) Auto-Rewind Spool Holder page.

Additional stands and other mods designed specifically for this Universal Auto-Rewinder by the awesome community:

[Prusa i3 MK3 top stand](https://www.thingiverse.com/thing:3342786)
[Stand for 3kg spools](https://www.thingiverse.com/thing:3369189)
[4-lobe NutCurved](https://www.thingiverse.com/thing:3382027)
[Yet another rewinding spoolholder](https://www.thingiverse.com/thing:3380568)
[Lack sliding spooler stand](https://www.thingiverse.com/thing:3378970)
[Spool hub spacer](https://www.thingiverse.com/thing:3386756)
[Numbered filament stands](https://www.thingiverse.com/thing:3383152)
[Three-pointed dial](https://www.thingiverse.com/thing:3396157)
[Remixed spoolholder](https://www.thingiverse.com/thing:3425043)
[Universal Auto-Rewind Spindle](https://www.thingiverse.com/thing:3446445)
[AutoRewind Spool Stand Hanging Screw Mount](https://www.thingiverse.com/thing:3474307)
[Adapter for Spannerhands Spool System](https://www.thingiverse.com/thing:3490353)
[3d printed 608 bearing](https://www.thingiverse.com/thing:3506177)
[RepRewind](https://www.thingiverse.com/thing:3516858)
[Remix with 3 sizes filament rings](https://www.thingiverse.com/thing:3528124)
[Stand for Wanhao Duplicator i3](https://www.thingiverse.com/thing:3539620)
[The MMU2s Prusa Final Filament Rewind Solution](https://www.thingiverse.com/thing:3573804)
[SpannerHands dual-color hub](https://www.thingiverse.com/thing:3608441)
[Wall Mount Spool Rewinder Adapter](https://www.thingiverse.com/thing:3619814)
[2020 extrusion top mount](https://www.thingiverse.com/thing:3627169)
[T-Slot Mount](https://www.thingiverse.com/thing:3634954)
[Wall-mount Auto-rewind Spool Thing](https://www.thingiverse.com/thing:3636609)
[Autorewind Hanger](https://www.thingiverse.com/thing:3554015)
[Hanging bracket](https://www.thingiverse.com/thing:3641466)
[Plain dial](https://www.thingiverse.com/thing:3642405)
[Prusa MK3 mount](https://www.thingiverse.com/thing:3642869)
[Wall mount](https://www.prusaprinters.org/prints/2909-spool-holder-for-auto-rewind-wall-mounted)

Version history
* v3.3 May 11, 2019
    - File section now has newest (v3.3) parts
    - Reorganized files section, every version has its own zip file with subdirectories
* v3.2.2 pre-release May 9, 2019 (tube holder only)
    - Redesign of tube holder, to allow different positions and angles of the filament guide
* v3.2.1 pre-release April 24, 2019 (download zip file)
    - Minor updates over v3.02 in springs and axle
* v3.02 pre-release April 10, 2019 (download zip file)
    - Renumbered major version number to 3, to better differentiate from original (1.xx) and parametric (2.xx) auto-rewind spool holders
    - Single, multi-material STL and STEP files
    - Improved springs for better printing
    - Axle: changed diameter from 7.8 to 7.7 mm, adjusted clip location to give hub more room
    - Clutch: dial numbers repeated at three positions, eliminated very small areas from 1st layer in multi-material version
    - Dial: three pointers, thinner structure to leave more room for spring
    - Spring: dual symmetric spring is now default, enlarged axle hole
    - Hub: reinforced base connection
    - NutCurved and NutCurvedXL: chamfered bottom of thread to match hub reinforcement
    - All files now available in STEP format
* v1.02 RC5 (stiff version) March 27, 2019
    - Added stiffer version of experimental symmetric dual serial spring
* v1.02 RC5 (soft spring only) March 17, 2019
    - Further improvements on experimental symmetric dual serial spring (reinforcements at base and easier separation) 
* v1.02 RC4 (soft spring only) March 13, 2019
    - Improved experimental symmetric dual serial spring, to ease the splitting of the spring and axle hub segments
* v1.02 RC3 (soft spring only) March 11, 2019
    - Improved experimental symmetric dual serial spring
* v1.02 RC2 (springs only) Feb 17, 2019
    - Redesigned experimental spring: symmetric dual serial spring system, direction independent and less prone to clutch jams; stiff and soft versions (flat and round) available
* v1.02 RC1 (springs only) Feb 4, 2019
    - New experimental spring design: symmetric dual parallel spring system, direction independent and better guarded against overloading
* v1.01 Jan 12, 2019
    - Added CurvedNutXL for mounting spools with large inner diameter
    - Changed text on one stand to "It's Rewind Time!"
    - Slightly decreased notch size in clutch to make dialing easier
    - Modified single-material gcode for better cross-compatibility
    - Re-exported all STLs (should no longer show errors in Slic3r)
* v1.0 Jan 6, 2019
    - First version

# Post-Printing

![Alt text](https://cdn.thingiverse.com/assets/71/a3/15/81/a6/07-DSC09487.JPG)
All rewinder's parts (except stand and XL curved nut) fit on a single buildplate with size 250x210 mm. Example GCode can be downloaded from http://www.vincentgroenhuis.nl/projects/universalautorewinder/ in single-color and multi-color versions. The spring and clutch are printed concentrically and need to be separated as the spring is assembled upside-down. The provided gcode files use spring v1.01, an experimental new version (v1.02) is available in the Files section.

![Alt text](https://cdn.thingiverse.com/assets/1e/18/56/24/33/08-DSC09489.JPG)
Align the pointer of the dial with the highest marker of the clutch (without spring) and carefully insert dial in clutch. After that, rotate dial back to zero. If this is difficult then you can use the spool hub to have a better grip on the clutch.

![Alt text](https://cdn.thingiverse.com/assets/e1/9b/42/5f/e2/DualSpring.JPG)
The new springs require a bit of work to separate the layers properly (see video). The object is split into three parts: the base with outer rim and two separate springs. The three parts are connected at the axle and/or the secondary rim. Check surfaces and grind if necessary after splitting to ensure smooth operation. There are two stypes: a stiff version (left) and a soft version (right). The soft version only works if friction is small and the spool is properly balanced (see video), in that case it is recommended to use the soft spring. If the spool is not balanced or friction is higher than in the video or the soft spring breaks then use the stiff one instead. Both springs come with flat axle hub by default, but round axle hub versions are also available in the zip files.

![Alt text](https://cdn.thingiverse.com/assets/9d/e7/df/e3/8f/RewinderSpring.png)
This is a schematic cross-section of the spring showing the connectivity of the shafts, spring blades, rims and base. The yellow sections are to be broken free, the red parts remain connected.

![Alt text](https://cdn.thingiverse.com/assets/d7/32/f7/23/de/09-DSC09494.JPG)
Insert spring. This is an older photo showing the original spring. If you use the new dual spring then the top looks different. The spring shaft goes through the dial.

![Alt text](https://cdn.thingiverse.com/assets/4b/34/1e/de/2b/10-DSC09507.JPG)
Insert two 608 bearings (e.g. 608ZZ or 608RS) in the spool hub. The bearings are more or less necessary, if you leave them out then there is in principle too much friction to rewind the spool properly. 3D printed bearings generally also have too much friction due to surface irregularities, although you can always try in combination with a stiff spring of course. If the 608 bearings do not roll smoothly then consider de-greasing it with IPA (isporopylalcohol) and re-lubricating it with light oil (see video). Lower the friction in the bearings means that you can use lower settings of the dial, reducing material stresses in the spring and increasing its lifetime.

![Alt text](https://cdn.thingiverse.com/assets/69/d2/2d/44/a1/11-DSC09509.JPG)
Attach the spool hub on the clutch. Make sure that all clips are fully pressed in.

![Alt text](https://cdn.thingiverse.com/assets/e4/ea/e3/21/f9/12-DSC09513.JPG)
Insert the axle. The axle must be able to slide easily through the spring and bearing holes; grind axle if necessary. Place the clip as shown. There should be a small gap between clip and bearing.

![Alt text](https://cdn.thingiverse.com/assets/a2/9b/83/d2/d5/13-DSC09515.JPG)
The filament guide tube (if used) can be inserted in the tube holder. This photo shows an older version, from v3.3 the tube holder has ten holes at different angles and positions. If you do not use a filament guide then do not use the holder. If insertion is difficult then you can wiggle a flat screwdriver in the slot to open it up a bit. Alternatively, you can use a 4 mm drill to enlarge the hole. On the other hand, if the tube is loose then you can use some tape to prevent it from falling out.

![Alt text](https://cdn.thingiverse.com/assets/c3/96/61/9b/40/14-DSC09516.JPG)
The hub, stand, tube holder and curved nut can be put together as shown. The other end of the filament guide tube is typically attached to the extruder or the MMU (which also stabilizes the tube holder), so it should be longer than pictured here. You can now adjust the tension of the clutch using the dial to get a proper rewinding and slipping action. Ideally you would aim for a minimum of 1.5 full rewinding revolutions consistently (also right after the clutch slips). But do not set the dial too high, otherwise the spring will get overloaded and eventually break due to material fatigue. If the dial gets stuck at a high setting then wind up the spring towards the outer rim as far as possible, this makes it easier to dial back down. In the photographed configuration the filament guide is above the spool and leads the filament to the left. Different positions and filament directions are also possible, the spring works both ways. After flipping the direction you may to recalibrate the clutch tension (not needed for v1.02 and beyond), as it will be different due to the changed spring behaviour. The hub should be able to spin with very little friction: give it a twist and it should oscillate a few times before coming to a halt (see video).

![Alt text](https://cdn.thingiverse.com/assets/40/fc/c8/ef/46/TubeHolder_v3.2.2.JPG)
From v3.2.2 a redesigned tube holder is included which allows to mount the filament tube at different angles (0, 10, 20, 30 and 40 degrees) and lateral positions (two positions per angle). This helps in reducing the risk of the filament jumping off the spool.

![Alt text](https://cdn.thingiverse.com/assets/76/b4/ca/48/c5/15-DSC09519.JPG)
In this configuration the tue tube holder is positioned below the spool. Again, the filament tube must run all way to the extruder or MMU, and if you are not using the tube then you must leave the tube holder out.

![Alt text](https://cdn.thingiverse.com/assets/4e/4e/2d/4e/cc/16-DSC09518.JPG)
Due to the spool rotation direction (counterclockwise as seen from this side) you need two nuts to secure it. The lips of the tube holder can be opened to make a snap-on style attachment which makes it easier to change spools in this configuration.