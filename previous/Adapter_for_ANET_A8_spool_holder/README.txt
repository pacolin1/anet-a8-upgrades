                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1975954
Adapter for ANET A8 spool holder by tm4n is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

I printed the "SpoolHolder ANET A8" by Madjestik58 (http://www.thingiverse.com/thing:1624641) and realized they were pretty wobbly and I would need a longer rod if I wanted to use them. So I designed an Adapter from the better designed "Anet A8 Spool Holder Redux" by dldesign to fix them to the display unit and make it possible to use them with the rod that came with the printer.

The result is good, but might need a little bit of sanding (or a hammer ;) ) to make the parts fit to the SpoolHolder. It has a hole for a screw to make it even sturdier, but you don't really need it.

Make sure you turn it on the flat side so it can be printed without supports.

# Print Settings

Printer: Anet A8 (Prusa i3 clone)
Rafts: No
Supports: No
Resolution: 0.3
Infill: 15