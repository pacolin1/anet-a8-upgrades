intersection()
{
    translate([62,.1,20])
        import("E3D_Fan_Duct.stl");

    cube([40,40,10]);
}

translate([0,0,10])
scale([1,1,7.5])
translate([0,0,-10])
intersection()
{
    translate([62,.1,20])
        import("E3D_Fan_Duct.stl");

    translate([0,0,10])
        cube([40,40,1]);
}

translate([0,0,6.5])
intersection()
{
    translate([62,.1,20])
        import("E3D_Fan_Duct.stl");

    translate([0,0,11])
        cube([40,40,20]);
}

