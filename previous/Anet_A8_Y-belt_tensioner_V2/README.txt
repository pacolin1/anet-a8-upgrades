                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2024293
Anet A8 Y-belt tensioner V2 by YoshiCat is licensed under the Creative Commons - Attribution - Share Alike license.
http://creativecommons.org/licenses/by-sa/3.0/

# Summary

Add new file for part 3 to made it stronger, now V2_3 ( old one seems to break with some people ) Part 3 needs to be print with 100% infill.
Add 2 new V2 files with improvement of the parts 1 and 2. (See last photo)
This new version makes the construction more rigid
If you use the new parts you will need a longer screw for the belt tension

This tensioner can be installed without any modification on the a8 and makes it easy to tighten the belt.
For mounting on the frame are four M3 x 18 screws and four M3 nuts needed. For the tension you need one M3 x 18 screw and 1 M3 locknut. And two more M3 x 20 with locknut to mount the bearings and the pivot point.
I used three M3 washers on the bearings, one between the bearings and the other two between the bearing and the frame.


# Print Settings

Printer: Anet A8
Resolution: 0,2
Infill: 50

Notes: 
Wall 1.6