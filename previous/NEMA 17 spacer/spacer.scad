// NEMA 17 spacer plate
//
// Scott Alfter
// 22 Mar 18

linear_extrude(10) // set thickness here
    difference()
    {
        polygon([[0,4.5],[4.5,0],[40,0],[44.5,4.5],[44.5,40],[40,44.5],[4.5,44.5],[0,40]]);
        translate([6.5,6.5,0])
            circle(d=3.4, $fs=0.01);
        translate([38,6.5,0])
            circle(d=3.4, $fs=0.01);
        translate([6.5,38,0])
            circle(d=3.4, $fs=0.01);
        translate([38,38,0])
            circle(d=3.4, $fs=0.01);
        translate([22.25,22.25,0])
            circle(d=23, $fs=0.01);
    }
