$fn=30;

difference()
{
    cube([31.5,25,25]);
    translate([3,14,0])
        cube([25.5,11,25]);

    translate([15.75,0,15])
    rotate([-90,0,0])
        cylinder(d=8.4, h=25);

    translate([11.55,0,6.5])
        cube([8.4,6,8]);

    translate([15.75,6,15])
    rotate([-90,0,0])
        cylinder(d=16.5, h=8);
}
