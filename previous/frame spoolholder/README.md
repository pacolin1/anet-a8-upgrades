Anet A8 Quick-Change Spoolholder
================================

This mashup of two existing designs speeds up filament changes.  No fasteners
need to be done or undone; just slide the spool onto or off of the roller.
Bearings minimize friction in the roller.

Build Instructions
------------------

Print the spool holder and the 70-mm roller (the 96-mm roller is too long).  
Insert 608ZZ bearings in the ends of the roller.

Mount the spool holder over the left side of the frame through; you'll
probably need to replace the original screws with longer screws.

Use the M8 threaded rod, nuts, and washers from the stock A8 spoolholder:
bolt it to the new spoolholder with most of the length facing forward, then
slide the roller onto the rod and secure it with a nut.

