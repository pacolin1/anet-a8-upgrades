module original()
{
	translate([100,-100,0])
		import("Anet_A8_Chain_Y_Frame_Mount_v2.stl");
}

module plate()
{
	difference()
	{
		translate([0,12.0,0])
		rotate([90,0,0])
		linear_extrude(3.0, convexity=10)
			polygon([[-42,0],[1.5,0],[1.5,14],[-20,14],[-20,43],[-42,43]]);
		
		union()
		{
			translate([-34,13,17.5])
			rotate([90,0,0])
			linear_extrude(5, convexity=10)
				circle(d=4, $fn=45);
				
			translate([-34,13,37])
			rotate([90,0,0])
			linear_extrude(5, convexity=10)
				circle(d=4, $fn=45);
		}
	}
}
	
module attachment()
{
	difference()
	{
		translate([3.4,1.5,0])
		rotate([0,0,-90])
		import("Anet_A8_Chain.stl");
		
		linear_extrude(15, convexity=10)
		polygon([[5,-8.5],[5,5],[-25,5],[-25,-8.5]]);
	}
	
}

//original();
plate();
attachment();

linear_extrude(2.3, convexity=10)
polygon([[1.5,-10],[1.5,12],[-20.5,12],[-20.5,-10]]);

translate([1.5,0,0])
rotate([0,-90,0])
linear_extrude(2.3, convexity=10)
polygon([[0,-10],[0,12],[14,12],[14,-10]]);

translate([-18.5,0,0])
rotate([0,-90,0])
linear_extrude(2.3, convexity=10)
polygon([[0,-10],[0,12],[14,12],[14,-10]]);

linear_extrude(2.3, convexity=10)
polygon([[-42,10],[-19,10],[-19,-13]]);