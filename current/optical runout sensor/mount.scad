$fn=60;

translate([0,-2,0])
%import("../AM8 mod/extruder mount/1_2020_Extruder_Motor_Mount.stl");

translate([0,4,0])
translate([0,0,21.5])
rotate([0,0,90])
%import("../runout sensor mount/MK8.stl");

module nema17_spacer(t)
{
    linear_extrude(t) // set thickness here
    difference()
    {
        polygon([[0,4.5],[4.5,0],[40,0],[44.5,4.5],[44.5,40],[40,44.5],[4.5,44.5],[0,40]]);
        translate([6.5,6.5,0])
            circle(d=3.4, $fs=0.01);
        translate([38,6.5,0])
            circle(d=3.4, $fs=0.01);
        translate([6.5,38,0])
            circle(d=3.4, $fs=0.01);
        translate([38,38,0])
            circle(d=3.4, $fs=0.01);
        translate([22.25,22.25,0])
            circle(d=23, $fs=0.01);
    }
}

translate([5.6,14.1,-10])
%cylinder(d=1.75, h=200);

translate([-22.5,4,-.5])
rotate([90,0,0])
  nema17_spacer(4);

translate([-6.75,21.5,54])
rotate([180,0,0])
%import("Optical endstop.stl");

difference()
{
    union()
    {
        translate([97.3,8.4,195])
        rotate([-90,0,0])
            import("filamentsensor_fixed.stl");

        translate([-12,0,40])
            cube([35,4,27]);

        translate([-12,4,47])
            cube([35,4.5,20]);

        translate([-12,8.4,51])
            cube([8.5,10,12]);

        translate([14.6,8.4,51])
            cube([8.5,10,12]);
    }

    translate([-4,20,57])
    rotate([90,0,0])
        cylinder(d=3.1, h=8);

    translate([15,20,57])
    rotate([90,0,0])
        cylinder(d=3.1, h=8);
}