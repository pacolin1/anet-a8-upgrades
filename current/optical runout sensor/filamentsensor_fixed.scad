$fn=30;

translate([-91.75,138,5.6])
difference()
{
    translate([91.75,-138,-5.6])
        import("filamentsensor.stl");

    translate([0,15,0])
    rotate([90,0,0])
        cylinder(d=2.75, h=30); // 2.6 mm kinda works for ABS; trying 2.75 for PETG

    translate([0,-3.2,0])
    rotate([90,0,0])
        cylinder(d2=2.5, d1=6, h=7);
}


