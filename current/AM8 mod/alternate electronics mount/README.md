RAMPS/Raspberry Pi/Dual MOSFET Mount for Anet A8
================================================

The original motherboard for my A8 has been getting flaky lately, so I
replaced it.  I needed something to hold it in place and the existing
options didn't quite do what I wanted, so I knocked this together in
OpenSCAD.  It holds an Arduino Mega (with a RAMPS stacked on top), a
Raspberry Pi Model B+ or later, and a couple of MOSFET boards (which I had
been using with the original motherboard).  It bolts up where the original
motherboard went, so you don't need to drill any new holes.  If you don't
like the placement of components (maybe you're not using external MOSFETs,
for instance), it's easy to move them around to get the configuration you
want.

This design has gone through a few iterations to get it to where I can bolt
it up and put it to use.  Still have to tidy up the cables, but everything's
up and running again.
