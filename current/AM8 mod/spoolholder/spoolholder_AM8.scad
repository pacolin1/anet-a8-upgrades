$fn=30;

translate([0,-40,0])
difference()
{
    cube([65,25,4]);
    translate([10,12.5,0])
        cylinder(d=5.5, h=4);
    translate([55,12.5,0])
        cylinder(d=5.5, h=4);
}

linear_extrude(4, convexity=10)
    polygon([[0,-15],[0,0],[20,25],[45,25],[65,0],[65,-15]]);

translate([0,0,4])
rotate([90,0,0])
linear_extrude(4, convexity=10)
    polygon([[0,0],[20,20],[20,0]]);

translate([45,0,4])
rotate([90,0,0])
linear_extrude(4, convexity=10)
    polygon([[0,0],[0,20],[20,0]]);

translate([24,0,4])
rotate([90,0,-90])
linear_extrude(4, convexity=10)
    polygon([[0,0],[40,0],[0,40]]);

translate([34.5,0,4])
rotate([90,0,-90])
linear_extrude(4, convexity=10)
    polygon([[0,0],[40,0],[0,40]]);

translate([45,0,4])
rotate([90,0,-90])
linear_extrude(4, convexity=10)
    polygon([[0,0],[40,0],[0,40]]);

translate([20,0,0])
{
    difference()
    {
        union()
        {
            difference()
            {
                union()
                {
                    difference()
                    {
                        cube([25,25,170]);
                        translate([3.5,4,0])
                            cube([18,21,170]);
                    }
                    translate([10.5,4,0])
                        cube([4,21,170]);
                    translate([0,0,148])
                        cube([25,25,4]);
                }
                translate([12.5,0,150])
                rotate([-90,0,0])
                    cylinder(d=8.4, h=25);
            }

            translate([12.5,3,150])
            rotate([-90,0,0])
            difference()
            {
                cylinder(d=16.4, h=22);
                cylinder(d=8.4, h=22);
            }
        }
        translate([8.3,0,141.5])
            cube([8.4,6,8]);
    }
}

