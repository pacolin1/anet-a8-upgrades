$fn=30;

difference()
{
    cube([49,49,2]);
    translate([24.5,24.5,0])
    {
        translate([-12,0,0])
            cylinder(d=4.5, h=2);
        translate([12,7.5,0])
            cylinder(d=4.5, h=2);
        translate([12,-7.5,0])
            cylinder(d=4.5, h=2);
    }
}

translate([-24,-5,-18.28])
difference()
{

            rotate([0,0,180])
            translate([23.5,-16,0])
                import("16_AM8_Right_Side_Y_Chain_Link.stl");

            translate([0,-22,0])
            cube([30,20,20]);

}

translate([-24,-10,-18])
{
    cube([5,3,20]);
    cube([18,3,3]);
}

translate([-6.5,-10,-18])
    cube([5,3,20]);

translate([-24,-10,-3])
    cube([24,22,5]);