$fn=30;

translate([0,12,0])
rotate([0,0,180])
difference()
{
    rotate([0,0,180])
    translate([23.5,-16,0])
        import("16_AM8_Right_Side_Y_Chain_Link.stl");

    cube([30,20,20]);
}

translate([-40,0,0])
difference()
{
    cube([40+77,12,13]);
    translate([0,1,1])
        cube([40+77,10,12]);
    translate([22,11,2])
        cube([13,1,9]);
}

translate([77,-19,0])
difference()
{
    cube([2,50,20]);
    translate([0,10,10])
    rotate([0,90,0])
        cylinder(d=5.5, h=2);
    translate([0,40,10])
    rotate([0,90,0])
        cylinder(d=5.5, h=2);
}

translate([-65,-50,0])
{
    difference()
    {
        cube([165,14.5,14]);
        translate([0,1,1])
            cube([165,12.5,13]);
    }

    translate([0,-25+7.25,0])
    difference()
    {
        cube([2,50,20]);
        translate([0,10,10])
        rotate([0,90,0])
            cylinder(d=5.5, h=2);
        translate([0,40,10])
        rotate([0,90,0])
            cylinder(d=5.5, h=2);
    }
}