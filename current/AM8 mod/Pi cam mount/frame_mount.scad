$fn=30;

difference()
{
    linear_extrude(20, convexity=10)
        polygon([[0,0],[-25,-25],[-25,-75],[-30,-75],[-30,-15],[-0,20]]);

    translate([-20,10,10])
    rotate([0,90,0])
    {
        cylinder(d=5.5, h=20);
        cylinder(d=12, h=16);
    }
}

translate([-25,-70,10])
rotate([0,90,0])
import("2_ballSocket.stl");