module m3_nut_fix()
{
    rotate([0,0,30])
    difference()
    {
        cylinder(d=8, h=3, $fn=6);
        cylinder(d=6.4, h=3, $fn=6);
    }
}

module m4_nut_fix()
{
    difference()
    {
        cylinder(d=10, h=14, $fn=6);
        cylinder(d=8.05, h=14, $fn=6);
    }
}

difference()
{
    union()
    {
        import("../e3d-v6-carriage/E3DV6_BasePlate_V4.stl");

        translate([5,7,0])
            m3_nut_fix();

        translate([10,17.5,0])
            m3_nut_fix();

        translate([5,28,0])
            m3_nut_fix();

        translate([57,7,0])
            m3_nut_fix();

        translate([52,17.5,0])
            m3_nut_fix();

        translate([57,28,0])
            m3_nut_fix();

        translate([5,41.75,2])
            m3_nut_fix();

        translate([57,41.75,2]) // this one was misaligned vertically...fixing here
            m3_nut_fix();
        translate([57,41.75,0])
            cylinder(d=8, h=2, $fn=6);
    }
    translate([57,41.75,0])
        cylinder(d=3.4, h=2, $fn=30);
}

translate([17,41.75,0])
    m4_nut_fix();

translate([45,41.75,0])
    m4_nut_fix();
