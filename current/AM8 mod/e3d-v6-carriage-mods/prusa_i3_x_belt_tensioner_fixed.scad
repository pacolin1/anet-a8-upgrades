$fn=30;

// fatten on bottom
difference()
{
    translate([0,0,-1.5])
        import("prusa_i3_x_belt_tensioner.stl");

    translate([-20,-10,0])
        cube([80,20,20]);
}

// fatten on top
difference()
{
    translate([0,0,1.5])
        import("prusa_i3_x_belt_tensioner.stl");

    translate([-20,-10,-10])
        cube([80,20,20]);
}

difference()
{
    import("prusa_i3_x_belt_tensioner.stl");

    // drill out screw hole
    translate([20,0,5])
    rotate([0,90,0])
        cylinder(d=3.5, h=20);

    // enlarge nut pocket
    translate([35.125,0,5])
        cube([2.7+.5,7.7,6+.5], center=true);
}

// translate([-10,3,5])
// rotate([90,0,0])
// cylinder(d=14.3, h=6);

// fatten at pulley end
translate([-16.5,-8.35,-1.5])
    cube([1.5,3.75,13]);

translate([-16.5,4.6,-1.5])
    cube([1.5,3.75,13]);