with_chain_mod();

module no_chain_mod()
{
    rotate([90,0,0])
    {
        difference()
        {
            intersection()
            {
                rotate([0,0,180])
                translate([-115.9,-112,0])
                    import("../e3d-v6-carriage/E3DV6_E3DHolder_with_Chain_V4.stl");

                cube([42,25.4,35]);
            }

            translate([15.2,12.4,0])
                cube([15,15,7]);
        }
    }
}

module with_chain_mod()
{
    rotate([90,0,0])
    {
        difference()
        {
            intersection()
            {
                rotate([0,0,180])
                translate([-115.9,-112,0])
                    import("../e3d-v6-carriage/E3DV6_E3DHolder_with_Chain_V4.stl");

                cube([42,25.4,35]);
            }

            translate([15.2,12.4,0])
                cube([15,15,7]);

            translate([0,13,7.95])
                cube([10,10,17]);
        }

        translate([12.5,12,5.5])
        rotate([90,0,180])
            link();
    }
}

module link()
{
    intersection()
    {
        translate([-13.5,24.25,0])
            import("../Anet_a8_X-axis_cable_chain_with_z_end-stop/files/Anet_A8_Chain.stl");

        cube([20,25,20]);
    }
}