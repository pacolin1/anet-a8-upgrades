// measured dimensions: 86.40 x 38.20 mm

width=86.90; // 86.40;  increased a bit for PETG
height=39.20; // 38.20;

hole_spacing=85.50;

slack=0.2;
thickness=4.0;
bracket_width=15;

$fn=30;

difference()
{
    union()
    {
        difference()
        {
            cube([width+slack+2*thickness, height+slack+2*thickness, bracket_width]);
            translate([thickness, thickness, 0])
                cube([width+slack, height+slack, bracket_width]);
            translate([(width+slack+2*thickness)/2-1, 0, 0])
                cube([2, thickness, bracket_width]);
        }

        translate([-5,0,0])
            cube([5,height+slack+2*thickness,bracket_width]);
    }

    translate([thickness,height+slack+2*thickness-10,bracket_width/2])
    rotate([0,-90,0])
    {
        cylinder(d=5.5, h=thickness+5);
        cylinder(d=12, h=5);
    }
    translate([thickness,height+slack+2*thickness-30,bracket_width/2])
    rotate([0,-90,0])
    {
        cylinder(d=5.5, h=thickness+5);
        cylinder(d=12, h=5);
    }
}

translate([(width+slack+2*thickness)/2-16, 0, 0])
difference()
{
    linear_extrude(bracket_width)
        polygon([[0,0],[15,0],[15,-10],[10,-10]]);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=6.8, $fn=6, h=12);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=3.4, $fn=30, h=15);
}

translate([(width+slack+2*thickness)/2+16,0,bracket_width])
rotate([0,180,0])
difference()
{
    linear_extrude(bracket_width)
        polygon([[0,0],[15,0],[15,-10],[10,-10]]);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=6.8, $fn=45, h=12);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=3.4, $fn=30, h=15);
}
