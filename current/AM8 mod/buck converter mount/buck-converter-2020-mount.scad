$fn=30;

difference()
{
    union()
    {
        translate([-.5,-3,2])
            import("buck-converter-v1.stl");

        translate([-25.25,-11.75,0])
            cube([50.5,23.5,3]);
    }

    translate([-10,0,0])
        cylinder(d=5.4, h=5);

    translate([10,0,0])
        cylinder(d=5.4, h=5);
}

