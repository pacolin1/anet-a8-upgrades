                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:3063430
Optical filament runout sensor, hybrid, with PTFE guides by dancem is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

Here is my implementation of this great model & idea: https://www.thingiverse.com/thing:3035553
_____________________
##### Update 2018-09-10: New model arrived! Now with the guides from PTFE tube (4mm)! Use it instead of old one.

Long testing of regular version showed, that after a few rolls of filament printed, plastic guides lost their slipping and add more load on the extruder, even with soft sensor spring. So I made another version without such issue.
Also, it allow to use wider range of the springs, as sensor will have less friction even with high spring pressure.

How to assemble:
1. Push PTFE tube into a hole (it will be easier if you will put piece of a filament into the tube, it will make it harder). There are three holes - two one the sides of the base and one in a lever (filament is passing on top of it, not in the inside);
2. Cut the tube sides with sharp knife;
3. Use phillips-head screwdriver to enlarge tube holes a little by pushing the screwdriver it it and making a few turns with a pressure;
4. Assemble the sensor;
5. Done!

_____________________
##### Update 2018-08-23: Nut tolerance for the endstop mount was reduced, re-download please and comment how it works for you.

_____________________
As you can see it is filament runout sensor based on optical endstop. I had used mechanical one, but after some time the switch broke and I got rare but very annoying false alarms. I searched for optical solution and found, that simple use of optical endstop as the sensor is not possible with transparent filaments. Later I found the solution from [Victor Lazaro](https://www.thingiverse.com/LazaroFilm/about) (thanks a lot!), to use mechanical lever with optical component. And it works perfect! 

So I made my own version of the same sensor but with a little improvements:
- symmetric endstop placement
- bigger endstop mount holes for more precise adjustment of the endstop
- support for the springs in different sizes
- mount points for M4 bolts
- hole for a fastener to fix the wires
- user-friendly lever and overall design

To assemble it you will need several M3 bolts and nuts, a spring and the endstop of course.

I did not checked on practice, but springs with diameter 3-8mm should fit. Please post the results in comments, I will adjust the model if needed.

To adjust pressure on a filament and simplify its passage you may need to cut your spring a little. But don't even try to use the springs for hotbed alignment, they are too hard.


And the most interesting part for some of you - how to make it working? Well, I shared my settings for Repetier Firmware. For other firmwares you will had to google for the instructions, but it should not be hard to setup anyway:
1. Assemble and attach the sensor;
2. Plug endstop into free endstop socket on a board;
3. Enable runout sensor option and tell firmware, what pin runout sensor should use (it will selected endstop pin).


Like it? Print it! Share it!
-----------------------------------------------
Want to see more models? Press the [**TIP Designer**](https://www.thingiverse.com/thing:3063430#tip) button! Every $ will be spent on a pizza for another sleepless night, when a new model is born! Thanks in advance!

# Print Settings

Printer: Prusa i3 Steel DIY
Rafts: Doesn't Matter
Supports: Yes
Resolution: N0.4/L0.24mm
Infill: 30%

Notes: 
There are overhangs in some places, so be sure to use the supports or powerful cooling.

# How I Designed This

<iframe src="//www.youtube.com/embed/kWxf9oMMxT8" frameborder="0" allowfullscreen></iframe>