$fn=60;

translate([0,-2,0])
%import("../AM8 mod/extruder mount/1_2020_Extruder_Motor_Mount.stl");

translate([0,4,0])
translate([0,0,21.5])
rotate([0,0,90])
%import("MK8.stl");

module nema17_spacer(t)
{
    linear_extrude(t) // set thickness here
    difference()
    {
        polygon([[0,4.5],[4.5,0],[40,0],[44.5,4.5],[44.5,40],[40,44.5],[4.5,44.5],[0,40]]);
        translate([6.5,6.5,0])
            circle(d=3.4, $fs=0.01);
        translate([38,6.5,0])
            circle(d=3.4, $fs=0.01);
        translate([6.5,38,0])
            circle(d=3.4, $fs=0.01);
        translate([38,38,0])
            circle(d=3.4, $fs=0.01);
        translate([22.25,22.25,0])
            circle(d=23, $fs=0.01);
    }
}

translate([111,-4,163])
rotate([0,90,90])
%import("../optomechanical-filament-runout-sensor/Optical_filament_runout_sensor_PTFE_v6_body.stl");

translate([-22.5,4,-.5])
rotate([90,0,0])
  nema17_spacer(4);

translate([0,-2,46])
    cube([44,12,4], center=true);

translate([19.5,2,42])
    cube([5,4,5], center=true);
    
translate([-19.5,2,42])
    cube([5,4,5], center=true);

translate([0,-6,62])
difference()
{
    cube([44,4,30], center=true);

    translate([-17.5,0,0.5])
    translate([0,5,0])
    rotate([90,0,0])
    {
        cylinder(d=3.5, h=10);
        translate([0,0,4])
            cylinder(d=6.8, h=3, $fn=6);
    }

    translate([16,0,-8.75])
    translate([0,5,0])
    rotate([90,0,0])
    {
        cylinder(d=3.5, h=10);
        translate([0,0,4])
            cylinder(d=6.8, h=3, $fn=6);
    }

    translate([16,0,9.75])
    translate([0,5,0])
    rotate([90,0,0])
    {
        cylinder(d=3.5, h=10);
        translate([0,0,4])
            cylinder(d=6.8, h=3, $fn=6);
    }
}